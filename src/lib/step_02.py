import logging
from pathlib import Path
from typing import Tuple, List

from lib.output_helpers import red_text, yellow_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress

logger = logging.getLogger(__name__)


def maybe_run_fastqc(input_folder: Path, output_folder: Path) -> Tuple[List[Path], List[Path], List[Path]]:
    def need_to_run_fastqc(primer_path: Path) -> bool:
        for suffix in ('.html', '.zip'):
            report_file = output_folder / primer_path.name.replace('.fastq.gz', '_fastqc' + suffix)
            if not report_file.exists():
                return True
        return False

    skipped: List[Path] = []
    successful: List[Path] = []
    failed: List[Path] = []

    output_folder.mkdir(parents=True, exist_ok=True)
    input_files = sorted_natural(input_folder.iterdir())
    for i, input_file in enumerate(input_files):  # run fastqc
        show_progress(i + 1, len(input_files), 'Generating quality reports')
        if not need_to_run_fastqc(input_file):
            skipped.append(input_file)
            continue
        proc = commands.run_fastqc(input_file, output_folder)
        out = [line.decode('utf-8') for line in proc.stdout.readlines()]
        err = [line.decode('utf-8') for line in proc.stderr.readlines()]
        # note: we can only depend on text output from both stderr and stdout
        # both are not used in a consistent manner :/
        # return codes are also 0 for some manners of failed processing.
        for output in (out, err):
            for line in output:
                if line.startswith("Analysis complete"):
                    successful.append(input_file)
                    break
                elif line.startswith("Failed to process"):
                    logger.error(red_text('\n'.join(output)))
                    failed.append(input_file)
                    break
    return skipped, successful, failed


def make_quality_reports(config: AcaciaConfig) -> None:
    """STEP 2
    Produces fastqc quality reports for each fastq file.
    """
    if not config.gen_quality_reports:
        logger.info("Step #2: skipped")
        return
    logger.info(yellow_text("Step #2: Quality Reports"))
    for primer in config.primers:
        output_folder = config.folders.fastqcreports / primer.name
        output_folder.mkdir(parents=True, exist_ok=True)
        primers_dir = config.folders.archive / primer.name
        maybe_run_fastqc(input_folder=primers_dir, output_folder=output_folder)
