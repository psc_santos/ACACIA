import logging
from pathlib import Path
from typing import List

import pandas as pd

from lib.file_utils import delete_folder_content, replace_if_exists
from lib.output_helpers import yellow_text, red_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress, count_records

MONITORING_SERIES_STEP_04 = 'MERGED'

logger = logging.getLogger(__name__)


def merge_with_flash(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    # (4) Uses flash to merge reads
    # (4.1) first determine the degree of overlap for each primer pair
    if not config.merge_reads:
        logger.info('skipping step #4')
        return
    logger.info(yellow_text('\nStep #4: Merging Reads'))
    # (4.2) runs FLASh
    if MONITORING_SERIES_STEP_04 not in monitoring_df:
        monitoring_df[MONITORING_SERIES_STEP_04] = pd.Series(index=monitoring_df.index, dtype='uint')
    for primer in config.primers:
        sub_folders = primer.folders.merge
        for folder_name in ("histograms", "logs", "merged", "not_merged"):
            root_output_folder = config.folders.merge / primer.name / folder_name
            root_output_folder.mkdir(exist_ok=True, parents=True)

        primer_folder = config.merge_input_folder / primer.name
        forward_inputs: List[Path] = sorted_natural(primer_folder.glob('*_R1_*'))

        for index, forward_input in enumerate(forward_inputs):
            show_progress(index + 1, len(forward_inputs), 'Merging')
            reverse_input = forward_input.parent / forward_input.name.replace('_R1_', '_R2_')
            tmp_folder = config.folders.merge / primer.name / 'tmp'
            if tmp_folder.is_dir():
                delete_folder_content(tmp_folder)
                tmp_folder.rmdir()
            output_prefix = forward_input.name.split('_', maxsplit=1)[0] + '.fastq'
            log_file = sub_folders.logs / (output_prefix + '.log')
            if log_file.is_file():
                continue
            p, prefix = commands.run_flash(
                tmp_folder,
                primer.merge_min_overlap,
                primer.merge_max_overlap,
                forward_input,
                reverse_input
            )
            proc_output = [line.decode('utf-8') for line in p.stdout.readlines()]

            for output_file in tmp_folder.iterdir():
                name = output_file.name
                if '.hist' in output_file.name:
                    replace_if_exists(output_file, sub_folders.histograms / name)
                elif '.notCombined' in output_file.name:
                    replace_if_exists(output_file, sub_folders.not_merged / name)
                elif '.extendedFrags' in output_file.name:
                    replace_if_exists(output_file, sub_folders.merged / name)
                else:
                    output_file.unlink()
                    logger.info(yellow_text('\nRemoved remaining file ' + str(output_file)))
            try:
                with open(str(log_file), 'wt') as out_file:
                    out_file.write(str('\n'.join(proc_output)))
            except KeyboardInterrupt:
                logger.info(red_text('aborting'))
                if log_file.is_file():
                    log_file.unlink()
                    logger.info('removed partial file:', str(log_file))
                raise

            tmp_folder.rmdir()

        logger.info(green_text("Saving results"))
        # todo run parallel
        for index, elem in enumerate(sub_folders.merged.iterdir()):
            show_progress(index + 1, len(forward_inputs), 'Merged')
            assert ".fastq.extendedFrags." in elem.name
            indiv = elem.name.split(".fastq.extendedFrags.")
            indiv = indiv[0]
            if (primer.name, indiv) in monitoring_df.loc[:, MONITORING_SERIES_STEP_04]:
                if 0 != monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_04]:
                    continue
            monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_04] = count_records(elem, 'fastq')

    tmp_mon = config.root_folder / 'mon_tmp.csv'
    monitoring_df.to_csv(str(tmp_mon))
    tmp_mon.replace(config.root_folder / 'mon.csv')
