# -*- coding: utf-8 -*-
import configparser
import argparse
import logging
import os
import textwrap
from typing import List, Tuple, TypeVar, Optional, Callable
from pathlib import Path
from shutil import which
from lib.helpers import unfold_primer

T = TypeVar('T')
logger = logging.getLogger(__name__)


class InvalidUserInput(argparse.ArgumentTypeError):
    def __init__(self, error_msg):
        self.message = error_msg

    def __str__(self):
        return self.message


def ask_user(validation_fn, message):
    """
    gather and validate input from the user
    :param validation_fn: a callable that will receive the user input,
    should raise InvalidUserException when unsuccessful
    :param message: the message to present to the user, asking for their input
    :return: the return value of the validation_fn
    """
    while True:
        try:
            return validation_fn(input(message))
        except EOFError:
            continue
        except InvalidUserInput as e_:
            print(e_)
            continue
        except KeyboardInterrupt:
            raise


def create_root_folder(path_str: str) -> Path:
    # (1) creates the basic folder system. Subfolders will be created later.
    if 0 == len(path_str):
        raise InvalidUserInput('input for work folder was empty')
    try:
        rootfolder = Path(path_str)
        rootfolder.mkdir(exist_ok=True, parents=True)
        return rootfolder
    except OSError as e_:
        raise InvalidUserInput(str(e_))


def validate_yes_no_question(user_input: str) -> bool:
    """
    validate user input for a yes or no question
    :param user_input: str
    :return: True if yes, False if no
    """
    msg = 'Please answer with exactly one character for "yes" (Y) or "no" (N)'
    if 1 != len(user_input):
        raise InvalidUserInput(msg)
    if user_input.upper() not in ('Y', 'N'):
        raise InvalidUserInput(msg)
    if 'Y' == user_input.upper():
        return True
    else:
        return False


def ignore_user_input(_: str) -> None:
    pass


def validate_primer_names(user_input: str) -> List[str]:
    if '' == user_input.strip():
        raise InvalidUserInput("please enter primer names")
    if "." in user_input:
        raise InvalidUserInput("primer names may not contain dots (.)")
    return user_input.strip().split(", ")


def validate_int_or_zero(user_input: str) -> int:
    if "" == user_input.strip():
        return 0
    try:
        return int(user_input.strip())
    except ValueError:
        raise InvalidUserInput("Please enter a number or hit ENTER")


def validate_filter_p(user_input: str) -> int:
    if "" == user_input.strip():
        return 90
    try:
        if 0 <= int(user_input.strip()) <= 100:
            return int(user_input.strip())
        else:
            raise InvalidUserInput(
                "p: please enter a positive number between 0 and 100, or hit ENTER to use the default")
    except ValueError:
        raise InvalidUserInput("Please enter a number for -p or hit ENTER to use the default")


def validate_filter_q(user_input: str) -> int:
    if "" == user_input.strip():
        return 30
    try:
        if 0 <= int(user_input.strip()) <= 40:
            return int(user_input.strip())
        else:
            raise InvalidUserInput("q: please enter a number between 0 and 40 or hit ENTER to use the default")
    except ValueError:
        raise InvalidUserInput("Please enter a number for -q or hit ENTER to use the default")


def validate_float(user_input: str) -> float:
    try:
        return float(user_input)
    except ValueError:
        raise InvalidUserInput(user_input + ' is not a valid float')


def validate_abs_nor(user_input: str) -> int:
    if "" == user_input.strip():
        return 10
    try:
        value = int(user_input.strip())
        if 0 > value:
            raise InvalidUserInput('the value must be positive')
        if 1000 < value:
            raise InvalidUserInput('the value must be <= 1000')
        return value
    except ValueError:
        raise InvalidUserInput("please enter a number or nothing")


def validate_low_por(user_input: str) -> float:
    if '' == user_input.strip():
        return 0.0
    value = validate_float(user_input)
    if 0 > value:
        raise InvalidUserInput('low_por must be positive')
    if 1 < value:
        raise InvalidUserInput('low_por must be lower than 1')
    return value


def validate_file_exists(user_input: str) -> Path:
    path = Path(user_input)
    if not path.is_file():
        raise InvalidUserInput("the file does not exist: " + user_input)
    return path


def validate_file_exists_or_empty(user_input: str) -> Optional[Path]:
    if '' == user_input.strip():
        return None
    return validate_file_exists(user_input)


def validate_sequence_expansion(user_input: str) -> str:
    if '' == user_input.strip():
        raise InvalidUserInput('please enter a valid primer sequence.')
    try:
        unfold_primer(user_input)
    except KeyError:
        raise InvalidUserInput(user_input + ' is not a valid DNA sequence')
    except TypeError:
        raise InvalidUserInput(user_input + ' is not a valid DNA sequence')
    return user_input


class SubFolders:
    def __init__(self, root: Path) -> None:
        self.root = root
        self.archive = root / "01_archive"
        self.fastqcreports = root / "02_fastqcreports"
        self.trimquality = root / "03_trimquality"
        self.merge = root / "04_merge"
        self.qualityfilter = root / "05_qualityfilter"
        self.collapse = root / "06_collapse"
        self.chimeras = root / "07_chimeras"
        self.blast = root / "08_blast"
        self.align = root / "09_align"
        self.expand = root / "10_expand"
        self.entropy = root / "11_entropy"
        self.oligotype = root / "12_oligotype"
        self.reports = root / "13_reports"


class PrimerFolders:
    def __init__(self, subfolders: SubFolders, primer: str) -> None:
        self.folders = subfolders
        self.primer = primer

    @property
    def trimquality(self):
        class TrimSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.trimmed = parent / primer / '01_trimmed'
                self.fastqcreports = parent / primer / '02_fastqcreports'

        return TrimSubFolders(self.folders.trimquality, self.primer)

    @property
    def merge(self):
        class MergeSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.histograms = parent / primer / 'histograms'
                self.not_merged = parent / primer / 'not_merged'
                self.merged = parent / primer / 'merged'
                self.logs = parent / primer / 'logs'

        return MergeSubFolders(self.folders.merge, self.primer)

    @property
    def qualityfilter(self):
        class QualityFilterSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.primerfiltered = parent / primer / '01_primerfiltered'
                self.highquality = parent / primer / '02_highquality'

        return QualityFilterSubFolders(self.folders.qualityfilter, self.primer)

    @property
    def collapse(self):
        class CollapseSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.collapsed = parent / primer / 'collapsed'

        return CollapseSubFolders(self.folders.collapse, self.primer)

    @property
    def chimeras(self):
        class ChimerasSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.vsearchresults = parent / primer / '01_vsearchresults'
                self.preliminary = parent / primer / '02_preliminary'
                self.nonchimeric = parent / primer / '03_nonchimeric'

        return ChimerasSubFolders(self.folders.chimeras, self.primer)

    @property
    def blast(self):
        class BlastSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.db = parent / primer / '00_db'
                self.blastresults = parent / primer / '01_blastresults'
                self.blastclean = parent / primer / '02_blastclean'

        return BlastSubFolders(self.folders.blast, self.primer)

    @property
    def align(self):
        class AlignSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.pooled = parent / primer / '01_pooled'
                self.aligned = parent / primer / '02_aligned'

        return AlignSubFolders(self.folders.align, self.primer)

    @property
    def expand(self):
        class ExpandSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.expanded = parent / primer / '01_expanded'

        return ExpandSubFolders(self.folders.expand, self.primer)

    @property
    def entropy(self):
        class EntropySubFolders:
            def __init__(self, parent: Path, primer: str):
                self.results = parent / primer / '01_results'

        return EntropySubFolders(self.folders.entropy, self.primer)

    @property
    def oligotype(self):
        class OligotypeSubFolders:
            def __init__(self, parent: Path, primer: str):
                self.results = parent / primer / '01_results'

        return OligotypeSubFolders(self.folders.oligotype, self.primer)


def make_tuple_parser(constructor: Callable[[str], T] = str, size: Optional[int] = None) -> Callable[[str], Tuple[T]]:
    def make_tuple(user_input: str) -> Tuple[T]:
        splits = user_input.split(',')
        if None is not size and len(splits) != size:
            raise InvalidUserInput(f'input: {user_input} does not match expected size: {str(size)}')
        try:
            return tuple([constructor(x) for x in splits])
        except Exception as e_:
            raise InvalidUserInput(str(e_))

    return make_tuple


class AcaciaConfig:
    def __init__(self):
        self.name = 'ACACIA'
        self._root_folder: Optional[Path] = None
        self.folders: Optional[SubFolders] = None
        self.merge_input_folder: Optional[Path] = None
        self.config_parser = configparser.RawConfigParser()
        self.primers: List[PrimerConfig] = []

        self._get_config = self.try_get_setting

        parser = argparse.ArgumentParser(description='ACACIA pipeline')
        parser.add_argument('folder', nargs='?', help='where to put your files')
        group = parser.add_argument_group(
            title='force flags',
            description='if you elected to skip a step (during a previous run), you can use these flags to run them'
        )
        group.add_argument('--merge', action='store_true', help='merge r1 and r2 files')
        group.add_argument('--filter', action='store_true', help='filter and discard sequences')
        # group.add_argument('--collapse', action='store_true', help='collapse sequences')
        group.add_argument('--quality', action='store_true', help='produce quality reports for your input files')
        group.add_argument('--quality-trim', action='store_true', help='produce quality reports for your trimmed files')
        args = parser.parse_args()
        if None is not args.folder:
            self.root_folder = Path(args.folder)

        if args.merge:
            self.merge_reads = True
        if args.filter:
            self.filter_sequences = True
        # if args.collapse:
        #     self.collapse_sequences = True
        if args.quality:
            self.gen_quality_reports = True
        if args.quality_trim:
            self.gen_trimmed_quality_reports = True

    def write_cfg(self):
        if None is self.root_folder:
            return
        if not self.root_folder.is_dir():
            return
        cfg_path = self.root_folder / 'config.ini'
        with open(str(cfg_path), 'wt') as cfg_file:
            self.config_parser.write(cfg_file)

    def try_get_setting(self, section, key, validation=None, kind=None, default=None):
        getters = {
            bool: self.config_parser.getboolean,
            None: self.config_parser.get,
            str: self.config_parser.get,
            int: self.config_parser.getint,
            float: self.config_parser.getfloat,
        }
        getter = getters[kind]
        try:
            value = getter(section, key)
            if None is validation:
                return value
            return validation(value)
        except configparser.NoOptionError:
            return default

    def _set_config(self, key, value):
        if not self.config_parser.has_section(self.name):
            self.config_parser.add_section(self.name)
        # welp, configparser tries to call bool.lower() which fails if it is not read back :/
        # maybe setting the values as str will clear the problem?
        self.config_parser.set(self.name, key, str(value))
        self.write_cfg()

    @property
    def root_folder(self):
        return self._root_folder

    @root_folder.setter
    def root_folder(self, path: Path):
        path.mkdir(parents=True, exist_ok=True)
        self._root_folder = path
        self.folders = SubFolders(path)
        self.merge_input_folder = self.folders.archive
        self.parse_cfg()

    @property
    def blast_reference_file(self) -> Optional[Path]:
        return self._get_config(self.name, 'blast_reference_file', validation=validate_file_exists)

    @blast_reference_file.setter
    def blast_reference_file(self, value: Path):
        self._set_config('blast_reference_file', value)

    @property
    def gen_quality_reports(self) -> Optional[bool]:
        return self._get_config(self.name, 'gen_quality_reports', kind=bool)

    @gen_quality_reports.setter
    def gen_quality_reports(self, value: bool):
        self._set_config('gen_quality_reports', value)

    @property
    def gen_trimmed_quality_reports(self) -> Optional[bool]:
        return self._get_config(self.name, 'gen_trimmed_quality_reports', kind=bool)

    @gen_trimmed_quality_reports.setter
    def gen_trimmed_quality_reports(self, value: bool):
        self._set_config('gen_trimmed_quality_reports', value)

    # @property
    # def collapse_sequences(self) -> Optional[bool]:
    #     return self._get_config(self.name, 'collapse_sequences', kind=bool)
    #
    # @collapse_sequences.setter
    # def collapse_sequences(self, value: bool):
    #     self._set_config('collapse_sequences', value)

    @property
    def merge_reads(self) -> Optional[bool]:
        return self._get_config(self.name, 'merge_reads', kind=bool)

    @merge_reads.setter
    def merge_reads(self, value: bool):
        self._set_config('merge_reads', value)

    @property
    def filter_sequences(self) -> Optional[bool]:
        return self._get_config(self.name, 'filter_sequences', kind=bool)

    @filter_sequences.setter
    def filter_sequences(self, value: bool):
        self._set_config('filter_sequences', value)

    def parse_cfg(self):
        if None is self.root_folder:
            return
        cfg_path = self.root_folder / 'config.ini'
        if not cfg_path.is_file():
            return

        self.config_parser.read(str(cfg_path))
        self.primers = []
        for section in self.config_parser.sections():
            if self.name == section:
                continue
            self.primers.append(PrimerConfig(section, self))

    def check(self):
        executables_to_check = ['fastqc', 'vsearch', 'mafft', 'entropy-analysis', 'oligotype', 'blastn', 'makeblastdb']
        missing_executables = []
        for ex in executables_to_check:
            if None is which(ex):
                missing_executables.append(ex)
        if 0 < len(missing_executables):
            logger.error('missing executables:', *missing_executables)
            raise SystemExit(1)

        if None is self.root_folder:
            self.root_folder = ask_user(create_root_folder, textwrap.dedent("""
            Please enter the full path of the working directory for this analysis (e.g. /home/user/myproject/).
            All new folders and files produced by ACACIA will be written into that directory:
            """))
        if not self.config_parser.has_section(self.name):
            self.config_parser.add_section(self.name)
        os.chdir(str(self.root_folder))

        """
        Read primer pairs from user, here: "ch1, ch2"
        for each primer:
            create dir, then
            Prompt user to extract/place files there, wait for RETURN
        """
        if 0 == len(self.primers):
            primer_names = ask_user(validate_primer_names, textwrap.dedent("""
            #1: Please enter the name of each primer PAIR used in the run that will be analyzed here.
            If you used more than one pair, separate their names with a comma and a space
            (f.ex. Primerpair1, Primerpair2, Primerpair3).
            Do not use dots (.) in the names:
            """))
            for name in primer_names:
                self.primers.append(PrimerConfig(name, self))
        for primer in self.primers:  # creates a subfolder for each primer pair in archive
            primer_path = self.folders.archive / primer.name
            if not primer_path.exists():
                primer_path.mkdir(parents=True)
                ask_user(ignore_user_input, textwrap.dedent(f"""
                Please copy (or move) the raw fastq.gz files generated with the primer 
                pair {primer.name} into the folder {primer_path}. Press ENTER when you are done.
                """))

        if None is which('fastqc'):
            logger.warning('fastqc executable not found, skipping quality report generation')
            self.gen_quality_reports = False  # step 2
            self.gen_trimmed_quality_reports = False  # step 3

        # step 2
        if None is self.gen_quality_reports:
            self.gen_quality_reports = ask_user(validate_yes_no_question, textwrap.dedent("""
            # 2: In the following step, quality reports can be generated for each fastq file,
            which can help you deciding to trim bases from your reads.
            Would you like ACACIA to produce quality reports of your fastq files (Y/N)?
            """))

    def check_trim(self):
        # step 3
        trim_count = 0
        ask_for_trim = False
        for primer in self.primers:
            if None is primer.trim_r1:
                ask_for_trim = True
                break
            if None is primer.trim_r2:
                ask_for_trim = True
                break
        if ask_for_trim:
            preamble = textwrap.dedent("""
            Based on the results of the quality reports (the previous step), you can now
            trim any number of bases from the end (right tip) of all R1 and R2 FASTQ files.
            Please enter here the number of bases to trim, or just press ENTER if nothing should be trimmed.
            Note that primer trimming will be performed at a later stage.
            """)
            print(preamble)
        for primer in self.primers:
            if None is primer.trim_r1:
                primer.trim_r1 = ask_user(
                    validate_int_or_zero,
                    f'Number of bases to trim from R1 read of {primer.name}: ')
                trim_count += primer.trim_r1
            if None is primer.trim_r2:
                primer.trim_r2 = ask_user(
                    validate_int_or_zero,
                    f'Number of bases to trim from R2 reads of {primer.name}: ')
                trim_count += primer.trim_r2
        if 0 < trim_count and None is self.gen_trimmed_quality_reports:
            self.merge_input_folder = self.folders.trimquality
            self.gen_trimmed_quality_reports = ask_user(validate_yes_no_question, textwrap.dedent("""
            You have chosen to trim raw reads. Would you like to generate fastqc reports
            for the new, trimmed fastq files? (Y/N):
            """))

    def check_merge(self):
        # step 4
        # todo: step 4.2 input folder, use trimmed data to merge or raw data?
        # todo: if trimmed data exists, favor that over raw data?
        if None is self.merge_reads:
            self.merge_reads = ask_user(validate_yes_no_question, textwrap.dedent("""
            The following step will merge your paired-end reads.
            Enter "Y" if you would like ACACIA to do it for you,
            or "N" if you did it already and want to skip this step (Y/N):
            """))
        if not self.merge_reads:
            return
        for primer in self.primers:
            # todo: check if min overlap <= max overlap?
            if None is primer.merge_min_overlap:
                primer.merge_min_overlap = ask_user(
                    validate_int_or_zero,
                    textwrap.dedent(f"""
                    Enter the MINIMAL number of bases expected to overlap when merging
                    forward and reverse reads of the primer pair {primer.name}:
                    """))
            if None is primer.merge_max_overlap:
                primer.merge_max_overlap = ask_user(
                    validate_int_or_zero,
                    textwrap.dedent(f"""
                    Enter the MAXIMAL number of bases expected to overlap when merging
                    forward and reverse reads of the primer pair {primer.name}:
                    """))

    def check_filter(self):
        if which('vsearch') is None:
            print('cannot run filter step without the vsearch executable')
            self.filter_sequences = False

        if None is self.filter_sequences:
            self.filter_sequences = ask_user(
                validate_yes_no_question,
                textwrap.dedent("""
                Step #5: This will now discard all reads missing perfect primers.
                Then, it will trim off all primers. Finally, it will filter sequences based on quality.
                Should ACACIA perform this step? (Y/N)
                """))

        for primer in self.primers:
            if None is primer.filter_forward:
                primer.filter_forward = ask_user(
                    validate_sequence_expansion,
                    textwrap.dedent(f"""
                    Please enter the sequence of the FORWARD primer in the 5' > 3' direction
                    for {primer.name}:
                    """))
            if None is primer.filter_reverse:
                primer.filter_reverse = ask_user(
                    validate_sequence_expansion,
                    textwrap.dedent(f"""
                    Please enter the sequence of the REVERSE primer in the 5' > 3' direction
                    for {primer.name}:
                    """))

        if None is self.blast_reference_file:
            text = textwrap.dedent("""
            A BLAST database will be created later for a BLAST filter.
            Please enter the path to a fasta file containing sequences that should be used as references.
            Your sequencing results will be BLASTed against them.
            (f.ex. /home/user/somefolder/relatedsequences.fasta).
            """)
            self.blast_reference_file = ask_user(validate_file_exists, text)
        for primer in self.primers:
            if None is primer.remove_singletons:
                text = textwrap.dedent(f"""
                As a general rule, should singletons (alleles yielded in one single file or PCR)
                be removed when processing primer {primer.name}? (Y/N):
                """)
                primer.remove_singletons = ask_user(validate_yes_no_question, text)

        ask_filter_qp = False
        for primer in self.primers:
            if primer.filter_p is None:
                ask_filter_qp = True
                break
            if primer.filter_q is None:
                ask_filter_qp = True
                break
        if ask_filter_qp:
            print(textwrap.dedent("""
            After primer trimming, sequences will be filtered based on quality.
            You can adjust here the "p" and "q" parameters:

            q: minimal Phred-score for quality filter.

            p: minimal percentage of the bases in a read that need to reach "q" (above),
                in order for the read to be kept.

            Hit ENTER to use the defaults.
            """))
            for primer in self.primers:
                if None is primer.filter_q:
                    primer.filter_q = ask_user(
                        validate_filter_q,
                        f'q for {primer.name} (default = 30): ')
                if None is primer.filter_p:
                    primer.filter_p = ask_user(
                        validate_filter_p,
                        f'p for {primer.name} (default = 90): ')

    def check_alleles(self):
        if not self.filter_sequences:
            return
        for primer in self.primers:
            if None is primer.alleles_locus:
                text = textwrap.dedent(f"""
                \n\nPlease enter the symbol of the locus being genotyped with {primer.name}
                (f.ex. Prlo-DRB, for the DRB gene of Procyon lotor).
                That symbol will be used for naming new alleles:
                """)
                primer.alleles_locus = input(text)
                # todo: 'test-locus' is a valid input, are there others? Do we want validation here?
        ask_abs_low = False
        for primer in self.primers:
            if None is primer.low_por:
                ask_abs_low = True
                break
            if None is primer.abs_nor:
                ask_abs_low = True
                break
        if ask_abs_low:
            print(textwrap.dedent("""
            You now need to set two thresholds:

            A) "abs_nor": "absolute smallest number of reads" that an allele has to reach in one ID,
               in order to be called in that ID. Independent of read depth.
               Range: Between 0 and 1000.
               Default = 10.

            B) "low_por": "lowest proportion of reads" supporting an allele in an amplicon,
               in order for that allele to be called in that ID.
               Range: Between 0 and 1.
               Default = 0.
               A low_por of 0.01 is recommended for larger data sets, which can suffer more from false positives.
               (read depth over 50,000X, or more than 200 IDs).
            """))
            for primer in self.primers:
                if None is primer.abs_nor:
                    primer.abs_nor = ask_user(validate_abs_nor, 'Your choice for abs_nor: ')
                if None is primer.low_por:
                    primer.low_por = ask_user(validate_low_por, 'Your choice for low_por: ')


class PrimerConfig:
    def __init__(self, name: str, acacia_conf: AcaciaConfig):
        self.name = name
        self._cfg_parser = acacia_conf.config_parser
        self._get_config = acacia_conf.try_get_setting
        self._ac_config = acacia_conf
        self.folders = PrimerFolders(acacia_conf.folders, self.name)
        if not self._cfg_parser.has_section(self.name):
            self._cfg_parser.add_section(self.name)
            self._ac_config.write_cfg()

    def _set_config(self, key, value):
        if not self._cfg_parser.has_section(self.name):
            self._cfg_parser.add_section(self.name)
        self._cfg_parser.set(self.name, key, str(value))
        self._ac_config.write_cfg()

    def __dict__(self):
        return {
            'trim_front': self.trim_r1,
            'trim_end': self.trim_r2,
            'merge_min_overlap': self.merge_min_overlap,
            'merge_max_overlap': self.merge_max_overlap,
            'filter_forward': self.filter_forward,
            'filter_reverse': self.filter_reverse,
            'filter_q': self.filter_q,
            'filter_p': self.filter_p,
            'known_alleles': self.known_alleles,
            'alleles_locus': self.alleles_locus,
            'remove_singletons': self.remove_singletons,
            'abs_nor': self.abs_nor,
            'low_por': self.low_por,
        }

    @property
    def trim_r1(self) -> Optional[int]:
        return self._get_config(self.name, 'trim_front', kind=int)

    @trim_r1.setter
    def trim_r1(self, value: int) -> None:
        self._set_config('trim_front', value)

    @property
    def trim_r2(self) -> Optional[int]:
        return self._get_config(self.name, 'trim_end', kind=int)

    @trim_r2.setter
    def trim_r2(self, value: int) -> None:
        self._set_config('trim_end', value)

    @property
    def merge_min_overlap(self) -> Optional[int]:
        return self._get_config(self.name, 'merge_min_overlap', kind=int)

    @merge_min_overlap.setter
    def merge_min_overlap(self, value: int) -> None:
        self._set_config('merge_min_overlap', value)

    @property
    def merge_max_overlap(self) -> Optional[int]:
        return self._get_config(self.name, 'merge_max_overlap', kind=int)

    @merge_max_overlap.setter
    def merge_max_overlap(self, value: int) -> None:
        self._set_config('merge_max_overlap', value)

    @property
    def filter_forward(self) -> Optional[str]:
        return self._get_config(self.name, 'filter_forward', kind=str)

    @filter_forward.setter
    def filter_forward(self, value: str) -> None:
        self._set_config('filter_forward', value)

    @property
    def filter_reverse(self) -> Optional[str]:
        return self._get_config(self.name, 'filter_reverse', kind=str)

    @filter_reverse.setter
    def filter_reverse(self, value: str) -> None:
        self._set_config('filter_reverse', value)

    @property
    def filter_p(self) -> Optional[int]:
        return self._get_config(self.name, 'filter_p', kind=int)

    @filter_p.setter
    def filter_p(self, value: int) -> None:
        self._set_config('filter_p', value)

    @property
    def filter_q(self) -> Optional[int]:
        return self._get_config(self.name, 'filter_q', kind=int)

    @filter_q.setter
    def filter_q(self, value: int) -> None:
        self._set_config('filter_q', value)

    @property
    def known_alleles(self) -> Optional[str]:
        return self._get_config(self.name, 'known_alleles', kind=str)

    @known_alleles.setter
    def known_alleles(self, value: str) -> None:
        self._set_config('known_alleles', value)

    @property
    def alleles_locus(self) -> Optional[str]:
        return self._get_config(self.name, 'alleles_locus', kind=str)

    @alleles_locus.setter
    def alleles_locus(self, value: str) -> None:
        self._set_config('alleles_locus', value)

    @property
    def remove_singletons(self) -> Optional[bool]:
        return self._get_config(self.name, 'remove_singletons', kind=bool)

    @remove_singletons.setter
    def remove_singletons(self, value: bool) -> None:
        self._set_config('remove_singletons', value)

    @property
    def abs_nor(self) -> Optional[float]:
        return self._get_config(self.name, 'abs_nor', kind=float)

    @abs_nor.setter
    def abs_nor(self, value: float) -> None:
        self._set_config('abs_nor', value)

    @property
    def low_por(self) -> Optional[float]:
        return self._get_config(self.name, 'low_por', kind=float)

    @low_por.setter
    def low_por(self, value: float) -> None:
        self._set_config('low_por', value)
