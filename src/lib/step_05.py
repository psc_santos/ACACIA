import gzip
import logging
from pathlib import Path
from typing import Optional, List

import pandas as pd
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

from lib.output_helpers import yellow_text, green_text, red_text
from lib.config import AcaciaConfig, PrimerConfig
from lib.helpers import sorted_natural, show_progress, rev_comp, unfold_primer, count_records, filter_quality

MONITORING_SERIES_STEP_05_1 = 'PRIMER_FILTERED'
MONITORING_SERIES_STEP_05_2 = 'QC_FILTERED'
logger = logging.getLogger(__name__)


def discard_trim_and_filter(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (5) First discards all reads missing primers, then trimms off the primers.
    Finally performs quality filter.
    :param config:
    :param monitoring_df:
    :return:
    """
    if not config.filter_sequences:
        logger.info(yellow_text('skipping discard -> trim -> filter (Step #5)'))
        return
    logger.info(yellow_text('Step #5: discard -> trim -> filter'))
    for series in (MONITORING_SERIES_STEP_05_2, MONITORING_SERIES_STEP_05_1):
        if series not in monitoring_df:
            monitoring_df[series] = pd.Series(index=monitoring_df.index, dtype='uint')
    for primer in config.primers:
        sub_folders = primer.folders.qualityfilter
        sub_folders.primerfiltered.mkdir(parents=True, exist_ok=True)
        sub_folders.highquality.mkdir(parents=True, exist_ok=True)

        merged_primer_folder = primer.folders.merge.merged
        merged_files = sorted_natural(merged_primer_folder.iterdir())
        # todo run parallel
        files_to_filter = list(find_files_to_filter_records_for(merged_files, sub_folders.primerfiltered))
        for index, (merged_input_path, output_path) in enumerate(files_to_filter):
            show_progress(index + 1, len(files_to_filter), 'Filtering')
            try:
                with gzip.open(str(output_path), "wt") as outfastq:
                    for record in filtered_record_generator(merged_input_path, primer):
                        SeqIO.write(record, outfastq, "fastq")
            except KeyboardInterrupt:
                logger.info('aborting and removing partial file: ' + str(output_path))
                if output_path.is_file():
                    output_path.unlink()
                raise

        logger.info(green_text(f'\nSaving primer filter results for {primer.name}'))
        filtered_primers = list(sub_folders.primerfiltered.iterdir())
        # todo parallel
        for index, filtered_primer in enumerate(filtered_primers):
            show_progress(index + 1, len(filtered_primers), 'Primer-trimmed results of ' + primer.name)
            assert ".fastq.gz" in filtered_primer.name
            indiv = filtered_primer.name.split(".fastq.gz", maxsplit=1)[0]
            if (primer.name, indiv) in monitoring_df.loc[:, MONITORING_SERIES_STEP_05_1]:
                if 0 != monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_05_1]:
                    continue
            monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_05_1] = count_records(filtered_primer,
                                                                                                 'fastq')
    tmp_mon = config.root_folder / 'mon_tmp.csv'
    monitoring_df.to_csv(str(tmp_mon))
    tmp_mon.replace(config.root_folder / 'mon.csv')

    for primer in config.primers:
        sub_folders = primer.folders.qualityfilter
        filtered_primer_files: List[Path] = sorted_natural(sub_folders.primerfiltered.iterdir())
        # todo parallel
        for index, fastqfile in enumerate(filtered_primer_files):
            fastafile = sub_folders.highquality / fastqfile.name.replace(".fastq.gz", ".fasta.gz")
            show_progress(index + 1, len(filtered_primer_files), 'Quality-filtered ' + primer.name)
            if fastafile.is_file():
                continue
            acceptable_quality_records = []
            with gzip.open(str(fastqfile), "rt") as infastq:
                for record in SeqIO.parse(infastq, "fastq"):
                    quality = record.letter_annotations["phred_quality"]
                    if filter_quality(quality, primer.filter_p, primer.filter_q) == 1:
                        acceptable_quality_records.append(record)
            try:
                with gzip.open(str(fastafile), "wt") as outfasta:
                    for record in acceptable_quality_records:
                        SeqIO.write(record, outfasta, "fasta")
            except KeyboardInterrupt:
                logger.info(red_text('aborting'))
                if fastafile.is_file():
                    fastafile.unlink()
                    logger.info('removed partial file: ', str(fastafile))
                raise

        logger.info(green_text(f'\nSaving quality control results for {primer.name}'))
        for qcedfile in sub_folders.highquality.iterdir():
            assert ".fasta.gz" in qcedfile.name
            indiv = qcedfile.name.split(".fasta.gz")
            indiv = indiv[0]
            if (primer.name, indiv) in monitoring_df.loc[:, MONITORING_SERIES_STEP_05_2]:
                if 0 != monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_05_2]:
                    continue
            with gzip.open(str(qcedfile), "rt") as infasta:
                k = len(list(SeqIO.parse(infasta, "fasta")))
                monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_05_2] = k
    tmp_mon = config.root_folder / 'mon_tmp.csv'
    monitoring_df.to_csv(str(tmp_mon))
    tmp_mon.replace(config.root_folder / 'mon.csv')


def filtered_record_generator(merged_input_path: Path, primer: PrimerConfig):
    with gzip.open(merged_input_path, "rt") as infastq:
        for rec in SeqIO.parse(infastq, "fastq"):
            for f_primer_seq in unfold_primer(primer.filter_forward):
                for r_primer_seq in unfold_primer(primer.filter_reverse):
                    filtered_record = filter_primer(f_primer_seq, r_primer_seq, rec)
                    if None is filtered_record:
                        # else try other direction:
                        filtered_record = filter_primer(r_primer_seq, f_primer_seq, rec)
                    if None is filtered_record:
                        continue
                    yield filtered_record
                    break


def find_files_to_filter_records_for(merged_files: List[Path], output_folder: Path):
    skipped = 0
    for merged_file in merged_files:
        output_name = merged_file.name.replace(".fastq.extendedFrags.fastq.gz", ".fastq.gz")
        output_path = output_folder / output_name
        if output_path.is_file():
            skipped += 1
            continue
        yield merged_file, output_path

    logger.info(f"skipped {skipped} already filtered files")


def filter_primer(forward_sequence: str, reverse_sequence: str, current_record: SeqRecord) -> Optional[SeqRecord]:
    current_sequence: Seq = current_record.seq
    start = current_sequence.find(forward_sequence)
    if -1 == start:
        return None
    end = current_sequence.find(rev_comp(reverse_sequence))
    if -1 == end:
        return None
    if not (start < end):
        return None
    filtered = current_record[:end]
    filtered = filtered[start + len(forward_sequence):]
    return filtered
