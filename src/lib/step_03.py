import gzip
import logging
from pathlib import Path

from Bio import SeqIO

from lib.output_helpers import yellow_text, red_text
from lib.step_02 import maybe_run_fastqc
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress

logger = logging.getLogger(__name__)


def trim_primers(config: AcaciaConfig) -> None:
    """STEP 3
    Offers to trim out the ends of forward (R1) and reverse reads (R2),
    based on the results of fastqc (2).
    Produces fastqc quality reports for each new fastq file.
    """
    trim_count = 0
    for primer in config.primers:
        trim_count += primer.trim_r1
        trim_count += primer.trim_r2
    if 0 == trim_count:
        # nothing to do.
        logger.info("Step #3: skipped")
        return
    logger.info(yellow_text("Step #3: Trimming Raw Reads"))
    for primer in config.primers:
        sub_folders = primer.folders.trimquality
        # trimmer
        input_folder = config.folders.archive / primer.name

        if 0 < primer.trim_r1:
            input_files = sorted_natural(input_folder.glob('*_R1_*'))
            sub_folders.trimmed.mkdir(exist_ok=True, parents=True)

            for i, input_file in enumerate(input_files):
                show_progress(i + 1, len(input_files), 'trim r1')
                trim(input_file, sub_folders.trimmed, primer.trim_r1)
        else:
            logger.info("Nothing to trim from the forward reads of " + primer.name)
        if 0 < primer.trim_r2:
            input_files = sorted_natural(input_folder.glob('*_R2_*'))
            sub_folders.trimmed.mkdir(exist_ok=True, parents=True)
            for i, input_file in enumerate(input_files):
                show_progress(i + 1, len(input_files), 'trim r2')
                trim(input_file, sub_folders.trimmed, primer.trim_r2)
        else:
            logger.info("Nothing to trim from the reverse reads of " + primer.name)

        # in case trimming was performed, produce fastqc quality reports for each new fastq file.
        if not config.gen_trimmed_quality_reports:
            continue
        sub_folders.fastqcreports.mkdir(exist_ok=True, parents=True)
        maybe_run_fastqc(input_folder, sub_folders.fastqcreports)


def trim(in_file: Path, out_dir: Path, trim_size: int) -> None:
    if (out_dir / in_file.name).exists():
        logger.info(f'trimmed file exists, skipping {in_file}')
        return
    trimmed_records = []
    with gzip.open(str(in_file), 'rt') as fastq_in:
        for seq_record in SeqIO.parse(fastq_in, 'fastq'):
            trimmed_records.append(seq_record[:-trim_size])
    out_path = out_dir / in_file.name
    try:
        with gzip.open(str(out_path), 'wt') as fastq_out:
            for record in trimmed_records:
                SeqIO.write(record, fastq_out, 'fastq')
    except KeyboardInterrupt:

        logger.info(red_text('aborting'))
        if out_path.is_file():
            out_path.unlink()
            logger.info('removed partial file:', str(out_path))
        raise
