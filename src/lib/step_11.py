import logging
from pathlib import Path
from typing import List

from lib.output_helpers import yellow_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress

logger = logging.getLogger(__name__)


def analyze_entropy(config: AcaciaConfig) -> None:
    """
    (11) This will run the entropy analyses
    :param config:
    :return:
    """
    # depends on collapse-chimera-blasting-alignment-expansion
    logger.info(yellow_text('Step #11: Entropy Analysis'))
    for primer in config.primers:
        sub_folders = primer.folders.entropy
        sub_folders.results.mkdir(exist_ok=True, parents=True)
        expanded_folder = primer.folders.expand.expanded
        if not expanded_folder.is_dir():
            logger.warning(f'nothing to do entropy analysis on, for primer {primer.name}')
            continue
        expanded_files: List[Path] = sorted_natural(expanded_folder.glob('*.fasta'))
        for i, expanded_file in enumerate(expanded_files):
            result_file = sub_folders.results / (expanded_file.name + '-ENTROPY')
            if result_file.is_file():
                continue
            proc = commands.run_entropy_analysis(expanded_file)
            errs = [line.decode('utf-8') for line in proc.stderr.readlines()]
            proc.wait()
            if 0 != proc.returncode:
                for error in errs:
                    logger.error(error)

            show_progress(i + 1, len(expanded_files), 'Entropy analysis of ' + primer.name)

        for expanded_file in expanded_folder.glob('*ENTROPY*'):
            expanded_file.rename(sub_folders.results / expanded_file.name)
        logger.info(green_text(f'Entropy analysis finished for {primer.name}'))
