from pathlib import Path


def delete_folder_content(folder: Path) -> None:
    if not folder.exists() or not folder.is_dir():
        return
    for item in folder.iterdir():
        if item.is_dir():
            delete_folder_content(item)
            item.rmdir()
            continue
        item.unlink()
        # add more where necessary


def replace_if_exists(old_path: Path, new_path: Path) -> None:
    if new_path.is_file():
        old_path.replace(new_path)
        return
    old_path.rename(new_path)
