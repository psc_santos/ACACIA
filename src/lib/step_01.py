import gzip
import math
from pathlib import Path
from typing import Tuple, Iterator, List

import pandas as pd
from Bio import SeqIO
import concurrent.futures as cf
import logging

from lib.commands import CPU_COUNT
from lib.output_helpers import green_text
from lib.config import AcaciaConfig, PrimerConfig
from lib.helpers import sorted_natural, show_progress

MONITORING_SERIES_STEP_01 = 'RAW_READS'
logger = logging.getLogger(__name__)


def build_monitoring_df(config: AcaciaConfig) -> pd.DataFrame:
    mon_path = config.root_folder / 'mon.csv'
    r1_paths: List[Tuple[PrimerConfig, Path]] = list(sorted_natural(r1_paths_with_paired_r2_sibling(config)))
    monitoring_df, raw_series = read_or_build_monitor(mon_path, build_index_tuples(r1_paths))

    def save():
        monitoring_df[MONITORING_SERIES_STEP_01] = raw_series
        tmp_mon = mon_path.parent / 'mon_tmp.csv'
        monitoring_df.to_csv(tmp_mon)
        tmp_mon.replace(mon_path)

    try:
        for (primer, individual, raw_reads) in count_raw_reads(r1_paths, raw_series):
            raw_series[(primer, individual)] = raw_reads

    except KeyboardInterrupt:
        logger.info("aborted: storing partially generated information")
        save()
        raise
    save()
    return monitoring_df


def read_sequence_count(primer: str, individual: str, fastq_path: Path):
    with gzip.open(fastq_path, 'rt') as fastq:
        return primer, individual, len(list(SeqIO.parse(fastq, "fastq")))


def all_unprocessed_fastq_files(r1_paths: List[Tuple[PrimerConfig, Path]], raw_series: pd.Series):
    logger.info(green_text(f'found {len(r1_paths)} pairs of FASTQ files'))
    already_processed = 0
    for (primer, fastq_path) in r1_paths:
        individual = get_individual_name_from(fastq_path)
        idx = (primer.name, individual)
        prev_raw_read_count = raw_series.get(idx, default=math.nan)
        if pd.isna(prev_raw_read_count):
            logger.debug(f'unprocessed: {fastq_path}')
            yield primer.name, individual, fastq_path
        else:
            logger.debug(f'already processed: {int(prev_raw_read_count)} raw reads from {fastq_path}')
            # already processed
            already_processed += 1
    logger.info(green_text(f'already read: {already_processed}'))


def count_raw_reads(r1_paths: List[Tuple[PrimerConfig, Path]], raw_series: pd.Series):
    need_to_process = list(all_unprocessed_fastq_files(r1_paths, raw_series))
    pending_count = len(need_to_process)
    try:
        with cf.ThreadPoolExecutor(max_workers=CPU_COUNT) as tpe:
            completed_count = 0
            jobs = [tpe.submit(read_sequence_count, *args) for args in need_to_process]
            for future in cf.as_completed(jobs):
                completed_count += 1
                show_progress(completed_count, pending_count, 'Processing raw data')
                yield future.result()
    except KeyboardInterrupt:
        logger.info('shutting down thread pool...')
        tpe.shutdown()
        raise


def r1_paths_with_paired_r2_sibling(config: AcaciaConfig) -> Iterator[Tuple[PrimerConfig, Path]]:
    for primer in config.primers:
        fastq_primer_root = config.folders.archive / primer.name
        r1_paths = fastq_primer_root.glob('*_R1_*')
        count_all = 0
        count_paired = 0
        for r1_path in r1_paths:
            count_all += 1
            r2_path = r1_path.parent / r1_path.name.replace('_R1_', '_R2_')
            if r2_path.is_file():
                count_paired += 1
                yield primer, r1_path
            else:
                logger.warning(
                    f'R1 path has no matching sibling, missing file: {r2_path}. ignoring {r1_path.name}'
                )
        logger.debug(f"detected: {count_all}, paired: {count_paired} in {fastq_primer_root}")


def get_individual_name_from(path: Path) -> str:
    return path.name.split("_", maxsplit=1)[0]


def build_index_tuples(inputs: List[Tuple[PrimerConfig, Path]]) -> Iterator[Tuple[str, str]]:
    for (primer, path) in inputs:
        yield primer.name, get_individual_name_from(path)


def read_or_build_monitor(mon_path: Path, index_builder: Iterator[Tuple[str, str]]) -> Tuple[pd.DataFrame, pd.Series]:
    multi_index_names = ['PRIMER', 'ID']

    def build_index():
        return pd.MultiIndex.from_tuples(index_builder, names=multi_index_names)

    if not mon_path.is_file():
        # build from scratch
        multi_index = build_index()
        return pd.DataFrame(index=multi_index), pd.Series(index=multi_index)
    logger.info(f'reading data from {mon_path}')
    df = pd.read_csv(mon_path).set_index(multi_index_names)
    if MONITORING_SERIES_STEP_01 in df:
        return df, df[MONITORING_SERIES_STEP_01]
    return df, pd.Series(index=build_index())
