import itertools
import logging
import textwrap

import numpy as np
import pandas as pd
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

from lib.output_helpers import yellow_text, red_text, green_text, blue_text, ICON
from lib.config import AcaciaConfig
from lib.helpers import sequence_compare, sorted_natural

logger = logging.getLogger(__name__)


def extract_alleles(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (13) filters out alleles and writes the report files
    This will now extract the alleles and write the reports for you.
    It will also name the alleles in decreasing order of abundance.
    :param config:
    :param monitoring_df:
    :return:
    """

    # depends on collapse-chimera-blasting-alignment-expansion-oligotype
    def calculate_abundances(oligo_cat):
        new_abundances = {}
        current_alleles = list(oligo_cat.SEQ)
        uniq_alleles = list((set(oligo_cat.SEQ)))
        for uniq_allele in uniq_alleles:
            allele_count = current_alleles.count(uniq_allele)
            new_abundances[uniq_allele] = allele_count

        if any([p.remove_singletons for p in config.primers]):
            logger.info(yellow_text("Removing singletons"))
            removed_singletons = 0
            for uniq_allele in new_abundances:
                if new_abundances[uniq_allele] == 1:
                    removed_singletons += 1
                    oligo_cat = oligo_cat[new_oligo_catalogue.SEQ != uniq_allele]
            logger.info(yellow_text(f"This step removed {removed_singletons} singletons."))
            uniq_allele_count = oligo_cat.SEQ.nunique()
            logger.info(yellow_text(f"There are now {uniq_allele_count} alleles among all individuals."))
        return new_abundances

    """
    (13.2) looks into oligotype results and creates a master dataframe with
    Oligos, IDs, Counts, Sequences and Aligned Sequences
    """
    for primer in config.primers:
        oligo_results_folder = primer.folders.oligotype.results
        tsv_path = oligo_results_folder / 'pooled' / 'ENVIRONMENT.txt'
        representatives_path = oligo_results_folder / 'pooled' / 'OLIGO-REPRESENTATIVES.fasta'
        if not tsv_path.is_file():
            logger.info(f'no such file: {tsv_path}')
            logger.error(red_text(
                f'The file ENVIRONMENT.txt could not be found. '
                f'You might want to re-run oligotype for primer: {primer.name}'))
            continue
        if not representatives_path.is_file():
            logger.error(red_text(
                f'The file OLIGO-REPRESENTATIVES.fasta could not be found. '
                f'You might want to re-run oligotype for primer: {primer.name}'))
            continue
        oligo_catalogue = pd.read_csv(str(tsv_path), sep="\t", header=None, engine="python")
        oligo_catalogue.columns = ["OLIGO", "ID", "OLIGOCOUNT"]
        oligos_n_sequences_dict = {}
        oligos_n_sequences_withgaps_dict = {}
        with open(str(representatives_path), "rt") as representatives:
            for record in SeqIO.parse(representatives, "fasta"):
                oligo = record.id
                seq = str(record.seq)
                oligos_n_sequences_dict[oligo] = seq.replace("-", "")
                oligos_n_sequences_withgaps_dict[oligo] = seq
        oligosandseqs = pd.DataFrame(oligos_n_sequences_dict.items(), columns=["OLIGO", "SEQ"])
        oligosandseqs_withgaps = pd.DataFrame(oligos_n_sequences_withgaps_dict.items(),
                                              columns=["OLIGO", "SEQ_ALIGNED"])
        oligo_catalogue = pd.merge(oligo_catalogue, oligosandseqs, on="OLIGO")
        oligo_catalogue = pd.merge(oligo_catalogue, oligosandseqs_withgaps, on="OLIGO")

        numberofuniquealleles = oligo_catalogue.SEQ.nunique()
        logger.info(f"The workflow produced {numberofuniquealleles} alleles so far considering all IDs.")

        # (13.3) builds dictionary with (preliminary) allele abundances. Remove singletons if wished.
        abundances = calculate_abundances(oligo_catalogue)
        abundances_df = pd.DataFrame(abundances.items(), columns=["SEQ", "ABUNDANCE"])

        # (13.4) looks at each individual separately
        abs_nor = primer.abs_nor
        low_por = primer.low_por
        new_oligo_catalogue = pd.DataFrame(columns=["OLIGO", "ID", "OLIGOCOUNT", "SEQ", "SEQ_ALIGNED"])
        indivs = list(set(oligo_catalogue.ID))
        for indiv in indivs:
            new_id_df = oligo_catalogue[oligo_catalogue.ID == indiv]
            new_id_df = new_id_df.sort_values(by=["OLIGOCOUNT"], ascending=False)
            # apply the abs_nor filter
            new_id_df = new_id_df[new_id_df.OLIGOCOUNT > abs_nor]
            localalleles = list(new_id_df.SEQ_ALIGNED)
            """
            filer out artifacts that differ from their "source" by
            only one nucleotide and that can be identified by frequency
            """
            for yelem, xelem in itertools.product(localalleles, localalleles):
                if localalleles.index(yelem) >= localalleles.index(xelem):
                    continue
                lowercount = new_id_df.OLIGOCOUNT[new_id_df.SEQ_ALIGNED == xelem]
                if len(lowercount) == 0:
                    continue
                proportion = lowercount.iloc[0] / sum(new_id_df.OLIGOCOUNT)
                if proportion < low_por:
                    new_id_df = new_id_df[new_id_df.SEQ_ALIGNED != xelem]  # apply the low_por filter
                    continue
                nrdiff = sequence_compare(xelem, yelem)
                if nrdiff != 1:
                    continue
                count_a = new_id_df.OLIGOCOUNT[new_id_df.SEQ_ALIGNED == yelem]
                count_b = new_id_df.OLIGOCOUNT[new_id_df.SEQ_ALIGNED == xelem]
                if len(count_a) == 0 or len(count_b) == 0:
                    continue
                if count_a.iloc[0] / count_b.iloc[0] <= 10:
                    continue
                if count_a.iloc[0] / count_b.iloc[0] <= 10:
                    continue
                # count of bigger allele is more than 10times the count of smaller allele IN THE INDIVIDUAL
                # count the oligos representing bigger allele in the pop
                count_biggerallele = int(sum(oligo_catalogue.OLIGOCOUNT[oligo_catalogue.SEQ_ALIGNED == yelem]))
                # count the oligos representing smaller allele in the pop
                count_smallerallele = int(sum(oligo_catalogue.OLIGOCOUNT[oligo_catalogue.SEQ_ALIGNED == xelem]))
                # count of bigger allele is more than 10times the count of smaller allele IN THE POPULATION.
                if count_biggerallele / count_smallerallele <= 10:
                    continue
                if abundances[xelem.replace("-", "")] >= 0.01 * sum(list(abundances_df["ABUNDANCE"])):
                    continue
                # now we have enough evidence to discard that smaller allele.
                new_id_df = new_id_df[new_id_df.SEQ_ALIGNED != xelem]

            potential_chimeras = {}
            for index1, row1 in new_id_df.iterrows():
                for index2, row2 in new_id_df.iterrows():
                    if not (row1["OLIGOCOUNT"] / row2["OLIGOCOUNT"] > 4):
                        continue
                    if index2 not in potential_chimeras:
                        potential_chimeras[index2] = [index1]
                    else:
                        potential_chimeras[index2].append(index1)

            for key, values in potential_chimeras.items():
                if not len(values) > 1:
                    continue
                try:
                    for elem in values:
                        if key not in list(new_id_df.index.values):
                            continue
                        child = new_id_df.at[key, "SEQ_ALIGNED"]
                        parent = new_id_df.at[elem, "SEQ_ALIGNED"]
                        if not (sequence_compare(child, parent) == sequence_compare(child[-5:], parent[-5:])):
                            continue
                        # all differences are concentrated in the last 5 bases only!
                        for item in potential_chimeras[key]:
                            # start looking for the other parent
                            other_parent = new_id_df.at[item, "SEQ_ALIGNED"]
                            if not (other_parent != parent):
                                continue
                            if not (child == parent[:-5] + other_parent[-5:]):
                                continue
                            # if this is true than we have found a late chimera
                            new_id_df = new_id_df[new_id_df.SEQ_ALIGNED != child]
                except:
                    # todo: what do we do here? some feedback to the user? what exceptions are expected?
                    pass

            new_oligo_catalogue = new_oligo_catalogue.append(new_id_df, ignore_index=True)

        """
        (13.5) removes very "thin" IDs.
        IDs need to have reached this point with at least 50 reads in order to make alleles
        """
        indivs = list(set(new_oligo_catalogue.ID))
        for indiv in indivs:
            sumofallreads = sum(new_oligo_catalogue.OLIGOCOUNT[new_oligo_catalogue.ID == indiv])
            if sumofallreads < 50:
                new_oligo_catalogue = new_oligo_catalogue[new_oligo_catalogue.ID != indiv]

        abundances = calculate_abundances(new_oligo_catalogue)
        abundances_df = pd.DataFrame(abundances.items(), columns=["SEQ", "ABUNDANCE"])
        abundances_df["ALLELE_NAMES"] = "NA"

        # (13.6) assigns new allele names based on abundance
        abundances_df = abundances_df.sort_values(by=["ABUNDANCE"], ascending=False)
        abundances_df = abundances_df.reset_index(drop=True)
        i = 1
        alleleprefix = primer.alleles_locus + '*'
        for index, row in abundances_df.iterrows():
            abundances_df.loc[abundances_df.SEQ == row["SEQ"], "ALLELE_NAMES"] = alleleprefix + f"{i:03d}"
            i += 1

        # (13.7) saves data into monitoring_df
        allele_count_series = pd.Series(index=monitoring_df.index, dtype='uint')
        for indiv in list(set(new_oligo_catalogue.ID)):
            allele_count_series[(primer.name, indiv)] = len(new_oligo_catalogue[new_oligo_catalogue.ID == indiv])
        monitoring_df['NR_OF_ALLELES'] = allele_count_series
        monitoring_df = monitoring_df.fillna(0)

        # (13.8) saves the pipeline report
        monitor_out_path = config.folders.reports / primer.name / 'pipelinereport.csv'
        monitor_out_path.parent.mkdir(parents=True, exist_ok=True)
        monitoring_df.to_csv(str(monitor_out_path), encoding="utf-8", index=True)

        # (13.9) saves detailed allele report
        allele_report_xl = new_oligo_catalogue.copy()
        allele_report_xl = allele_report_xl.drop(["OLIGO", "SEQ_ALIGNED"], axis=1)
        allele_report_xl["ALLELE"] = "NA"
        for index, row in allele_report_xl.iterrows():
            seq = row["SEQ"]
            name = abundances_df.ALLELE_NAMES[abundances_df.SEQ == seq]
            allele_report_xl.loc[allele_report_xl.SEQ == seq, "ALLELE"] = list(name)[0]
        allele_report_xl = allele_report_xl.rename(index=str, columns={"OLIGOCOUNT": "COUNT"})
        allele_report_xl = allele_report_xl[["ID", "ALLELE", "COUNT", "SEQ"]]
        allelereport_xl_path = config.folders.reports / primer.name / 'allelereport_XL.csv'
        allele_report_xl.to_csv(str(allelereport_xl_path), encoding="utf-8", index=False)

        # (13.10) saves the canonical allele report
        canonical_allele_report_path = config.folders.reports / primer.name / 'allelereport.csv'
        try:
            with open(str(canonical_allele_report_path), "w") as outfile:
                indivs = list(set(new_oligo_catalogue.ID))
                indivs = sorted_natural(indivs)
                print("This is an allele report. There are", len(indivs), "individuals here.\n", file=outfile)
                print("Population Report:\nAllele,Abundance,Sequence", file=outfile)
                for index, row in abundances_df.iterrows():
                    print(str(row["ALLELE_NAMES"]) + "," + str(row["ABUNDANCE"]) + "," + str(row["SEQ"]), file=outfile)
                print("\nIndividual Report:\nIndividual,Nr_of_alleles,Alleles", file=outfile)
                numberofalleles = []
                for individual in indivs:
                    individualallelelist = [allele for allele in
                                            list(allele_report_xl.ALLELE[allele_report_xl.ID == individual])]
                    sorted_natural(individualallelelist)
                    print(str(individual) + "," + str(len(individualallelelist)) + "," + ";".join(individualallelelist),
                          file=outfile)
                    numberofalleles.append(len(individualallelelist))
                print("\nThere are between", np.min(numberofalleles), "and", np.max(numberofalleles),
                      "alleles in the individuals here. The mean is", np.mean(numberofalleles), "and the SD is " +
                      str(np.std(numberofalleles)) + ".", end="", file=outfile)
        except KeyboardInterrupt:
            logger.info(red_text('aborting'))
            if canonical_allele_report_path.is_file():
                canonical_allele_report_path.unlink()
                logger.info(f'removed partial file: {canonical_allele_report_path}')
            raise
        # (13.11) writes a fasta file with the alleles
        allele_fasta_path = config.folders.reports / primer.name / (primer.alleles_locus + '.fasta')
        try:
            with open(str(allele_fasta_path), "wt") as outfasta:
                for index, row in abundances_df.iterrows():
                    record = SeqRecord(Seq(row["SEQ"]), row["ALLELE_NAMES"], '', '')
                    SeqIO.write(record, outfasta, "fasta")
        except KeyboardInterrupt:
            logger.info(red_text('aborting'))
            if allele_fasta_path.is_file():
                allele_fasta_path.unlink()
                logger.info(f'removed partial file: {allele_fasta_path}')
            raise
    logger.info(green_text(textwrap.dedent(f"""
    
    
    This is the end of the ACACIA pipeline!
    Your results are waiting for you in the folder {config.folders.reports}
    """)))
    logger.info(blue_text(ICON))
