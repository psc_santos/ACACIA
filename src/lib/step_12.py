import logging

import pandas as pd

from lib.output_helpers import yellow_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural
from lib.step_08 import MONITORING_SERIES_STEP_08

logger = logging.getLogger(__name__)


def oligotype_analysis(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (12) run the oligotype analyses
    :param config:
    :param monitoring_df:
    :return:
    """
    # depends on collapse-chimera-blasting-alignment-expansion-entropy
    for primer in config.primers:
        sub_folders = primer.folders.oligotype
        sub_folders.results.mkdir(exist_ok=True, parents=True)

        # (12.1) finds the right components based on the individual entropy files
        entropy_threshold = 0.15
        components_list = []
        entropy_paths = list(primer.folders.entropy.results.glob('*-ENTROPY'))
        if 0 == len(entropy_paths):
            logger.info(f'could not find any entropy files for primer {primer.name}, skipping oligotype analysis')
            continue
        for entropy_path in entropy_paths:
            if entropy_path.name.startswith('pooled'):
                continue
            with open(str(entropy_path)) as entropyfile:
                for line in entropyfile:
                    line = line.rstrip("\n")
                    line = line.split("\t")
                    component = line[0]
                    entropy = float(line[1])
                    if entropy >= entropy_threshold:
                        if component not in components_list:
                            components_list.append(component)
        components_list = sorted_natural(components_list)
        selected_components = ",".join(components_list)

        # (12.2) runs oligotype on the pooled file only, using the components list
        series = monitoring_df[MONITORING_SERIES_STEP_08][primer.name]  # pd.Series
        totalnrofreads = series.sum()
        # totalnrofreads = sum(list(monitoring_df.BLASTED[monitoring_df.PRIMER == primer]))
        file_count = len(list(primer.folders.blast.blastclean.iterdir()))
        if 0 == file_count:
            logger.info(f'Oligotype Analysis skipped for {primer.name}, no input files')
            continue
        min_actual_abundance = 0.1 * totalnrofreads / file_count
        min_actual_abundance = int(round(min_actual_abundance, 0))
        logger.info(yellow_text(f'Performing oligotype analysis on the pooled file of {primer.name}'))
        alignment_file = primer.folders.expand.expanded / 'pooled.fasta'
        entropy_file = primer.folders.entropy.results / 'pooled.fasta-ENTROPY'
        output_folder = sub_folders.results / 'pooled'
        commands.run_oligotype(
            alignment_file,
            entropy_file,
            output_folder,
            selected_components,
            min_actual_abundance
        )
        logger.info(green_text(f'Oligotype analysis finished for {primer.name}'))
