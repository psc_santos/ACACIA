import logging
import time
from pathlib import Path
from typing import List

import pandas as pd
from Bio import SeqIO

from lib.output_helpers import red_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress, fasta_sum_size
from lib.step_07 import MONITORING_SERIES_STEP_07

MONITORING_SERIES_STEP_08 = 'BLASTED'
logger = logging.getLogger(__name__)


def blast_sequences(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (8) This will now BLAST your sequences against a local database.
    :param config:
    :param monitoring_df:
    :return:
    """
    # depends on collapse/chimera filter
    if MONITORING_SERIES_STEP_07 not in monitoring_df:
        logger.info('skipping sequence BLASTing, chimera detection did not run?')
        return
    if MONITORING_SERIES_STEP_08 not in monitoring_df:
        monitoring_df[MONITORING_SERIES_STEP_08] = pd.Series(index=monitoring_df.index, dtype='uint')

    for primer in config.primers:
        sub_folders = primer.folders.blast
        for sub_folder in (sub_folders.blastclean, sub_folders.blastresults, sub_folders.db):
            sub_folder.mkdir(exist_ok=True, parents=True)
        # (1.) set up database
        db_output = sub_folders.db / primer.name
        process = commands.run_makeblastdb(config.blast_reference_file, db_output)
        for line in process.stdout.readlines():
            logger.info(line.decode("utf-8").rstrip("\n"))
        iterator = primer.folders.chimeras.nonchimeric.iterdir()
        non_chimeric_files: List[Path] = sorted_natural(iterator)
        # (2.) BLAST
        files_to_blast = list(find_files_that_need_blasting(non_chimeric_files, sub_folders.blastresults))
        for i, (non_chimeric_file, blast_output) in enumerate(files_to_blast):
            show_progress(i + 1, len(files_to_blast), f'blasting {primer.name}')
            proc = commands.run_blastn(db_output, non_chimeric_file, blast_output)
            proc.wait()
            # for line in proc.stdout.readlines():
            # logger.info(line.decode("utf-8"),)
        # (3.) filters blast results
        # todo run parallel
        time.sleep(5)  # make sure BLAST is done writing all files
        files_to_clean = list(find_files_that_need_cleaning(
            non_chimeric_files,
            sub_folders.blastclean,
            sub_folders.blastresults
        ))
        for i, build_clean_args in enumerate(files_to_clean):
            show_progress(i + 1, len(files_to_clean), f'Filtering {primer.name}')
            build_clean_output(*build_clean_args)

        logger.info(green_text("BLAST filter done."))
        cleaned_files = list(sub_folders.blastclean.iterdir())
        for index, cleaned_file in enumerate(cleaned_files):
            show_progress(index + 1, len(cleaned_files), f'Saving BLAST survivors for {primer.name}')

            # todo: this assert fails, should the blast outputs have the .fasta ext?
            # assert '.fasta' in cleaned_file.name
            indiv = cleaned_file.name.split(".fasta")[0]
            if (primer.name, indiv) in monitoring_df.loc[:, MONITORING_SERIES_STEP_08]:
                if 0 != monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_08]:
                    continue
            monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_08] = fasta_sum_size(cleaned_file)

    tmp_mon = config.root_folder / 'mon_tmp.csv'
    monitoring_df.to_csv(str(tmp_mon))
    tmp_mon.replace(config.root_folder / 'mon.csv')


def generate_clean_outputs(blast_output: Path, non_chimeric_file: Path, threshold=1e-20):
    with open(blast_output) as blastresultfile, open(non_chimeric_file, "rt") as infasta:
        blastdict = {}
        for line in blastresultfile:
            line = line.rstrip("\n")
            line = line.replace(";size=", ",")
            line = line.replace(";,", ",")
            line = line.split(",")
            identifier = line[0]
            size = int(line[1])
            evalue = float(line[2])
            if evalue < threshold and identifier not in blastdict:
                blastdict[identifier] = size
        for record in SeqIO.parse(infasta, "fasta"):
            ident = record.id.split(";")[0]
            if ident in blastdict:
                yield record


def build_clean_output(clean_output: Path, blast_output: Path, non_chimeric_file: Path):
    if clean_output.is_file():
        logger.info(f"clean output file exists, skipping regeneration: {clean_output}")
        return
    try:
        with open(clean_output, "wt") as outfasta:
            for record in generate_clean_outputs(blast_output, non_chimeric_file):
                SeqIO.write(record, outfasta, "fasta")
    except KeyboardInterrupt:
        logger.info(red_text('aborting'))
        if clean_output.is_file():
            clean_output.unlink()
            logger.info('removed partial file:', clean_output)
        raise


def find_files_that_need_blasting(non_chimeric_files: List[Path], output_folder: Path):
    already_blasted = 0
    for non_chimeric_file in non_chimeric_files:
        blast_output = output_folder / non_chimeric_file.name.rstrip(".fasta")
        if blast_output.is_file():
            already_blasted += 1
            continue
        yield non_chimeric_file, blast_output
    logger.info(f'skipped already blasted files: {already_blasted}')


def find_files_that_need_cleaning(non_chimeric_files: List[Path], output_folder: Path, blast_results_path: Path):
    already_cleaned = 0
    for i, non_chimeric_file in enumerate(non_chimeric_files):
        clean_output = output_folder / non_chimeric_file.name.rstrip(".fasta")
        if clean_output.is_file():
            already_cleaned += 1
            continue
        blast_output = blast_results_path / non_chimeric_file.name.rstrip(".fasta")
        yield clean_output, blast_output, non_chimeric_file
    logger.info(f'skipped already cleaned files: {already_cleaned}')
