import gzip
import logging
from pathlib import Path
from typing import List

import pandas as pd
from Bio import SeqIO

from lib.output_helpers import yellow_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress

MONITORING_SERIES_STEP_06 = 'WITHOUT_SINGLETONS'

logger = logging.getLogger(__name__)


def find_files_to_collapse(input_files: List[Path], collapsed_output_folder: Path):
    skipped = 0
    for input_file in input_files:
        output_file = collapsed_output_folder / input_file.name
        if output_file.is_file():
            skipped += 1
            continue
        yield input_file, output_file
    logger.info(f'skipped {skipped} already collapsed files')


def collapse_sequences(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (6) COLLAPSE: Collapses (dereplicates) all files and gets rid of all reads that appear only once in an individual.
    Files will be ready for chimera detection (VSEARCH) after this.
    :param config:
    :param monitoring_df:
    :return:
    """
    if not config.filter_sequences:
        logger.info('Skipped collapsing sequences, you might need to run with --filter first')
        return
    if MONITORING_SERIES_STEP_06 not in monitoring_df:
        monitoring_df[MONITORING_SERIES_STEP_06] = pd.Series(index=monitoring_df.index, dtype='uint')
    logger.info(yellow_text('\nStep #6: Collapsing Sequences'))
    # todo: depends on step5
    for primer in config.primers:
        collapsed_output_folder = primer.folders.collapse.collapsed
        collapsed_output_folder.mkdir(parents=True, exist_ok=True)

        high_quality_folder = primer.folders.qualityfilter.highquality
        input_files: List[Path] = sorted_natural(high_quality_folder.iterdir())
        need_to_be_vsearched = list(find_files_to_collapse(input_files, collapsed_output_folder))
        for i, (input_file, output_file) in enumerate(need_to_be_vsearched):
            show_progress(i + 1, len(need_to_be_vsearched), f'collapsing {primer.name}')
            proc = commands.run_vsearch_collapse(input_file, output_file)
            if output_file.is_file():
                continue
            err = [line.decode("utf-8") for line in proc.stderr.readlines()]
            if 0 != proc.returncode:
                for line in err:
                    if '' == line.strip():
                        continue
                    logger.error(line)
        logger.info(green_text("Collapsing done. Saving results for " + primer.name))
        gzipped_files = list(collapsed_output_folder.iterdir())
        for index, gzipped_file in enumerate(gzipped_files):
            show_progress(index + 1, len(gzipped_files), 'saving results for ' + primer.name)
            k = 0
            assert '.fasta' in gzipped_file.name
            indiv = gzipped_file.name.split(".fasta")[0]
            if (primer.name, indiv) in monitoring_df.loc[:, MONITORING_SERIES_STEP_06]:
                if 0 != monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_06]:
                    continue
            with gzip.open(str(gzipped_file), "rt") as infasta:
                for record in SeqIO.parse(infasta, "fasta"):
                    name = record.id
                    name = name.replace("=", ";")
                    name = name.split(";")
                    size = int(name[2])
                    k += size
            monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_06] = k
    tmp_mon = config.root_folder / 'mon_tmp.csv'
    monitoring_df.to_csv(str(tmp_mon))
    tmp_mon.replace(config.root_folder / 'mon.csv')
