import logging
from pathlib import Path
from typing import List

import pandas as pd
from Bio import SeqIO

from lib.output_helpers import red_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress, fasta_sum_size
from lib.step_06 import MONITORING_SERIES_STEP_06

MONITORING_SERIES_STEP_07 = 'NON_CHIMERIC'

logger = logging.getLogger(__name__)


def find_files_that_need_vsearching(collapsed_files: List[Path], singleton_series: pd.Series, primer_name: str, vsearch_output_path: Path, prelim_output_path: Path):
    already_searched = 0
    for i, collapsed_file in enumerate(collapsed_files):
        indiv = collapsed_file.name.split(".fasta")[0]
        # todo chimera detection breaks when name contains dash? -
        if singleton_series[(primer_name, indiv)] == 0:
            already_searched += 1
            continue
        chimera_file = vsearch_output_path / (indiv + '.out')
        preliminary_file = prelim_output_path / (indiv + '.fasta')
        if chimera_file.is_file() and preliminary_file.is_file():
            already_searched += 1
            continue
        yield collapsed_file, chimera_file, preliminary_file
    logger.info(f"skipping regeneration of {already_searched} already searched files")


def filter_acceptable(shit_list, fasta_file: Path):
    with open(fasta_file, "rt") as infasta:
        for record in SeqIO.parse(infasta, "fasta"):
            if record.id not in shit_list:
                yield record

def generate_acceptable_records(preliminary_files: List[Path], output_folder: Path, vsearch_result_path: Path):
    skipped = 0
    columns = [f"u{j + 1}" for j in range(18)]
    for preliminary_file in preliminary_files:
        out_file = output_folder / preliminary_file.name
        if out_file.exists():
            skipped += 1
            continue
        indiv = preliminary_file.name.split(".fasta")[0]
        chimera_file = vsearch_result_path / (indiv + '.out')
        if chimera_file.stat().st_size == 0:
            skipped += 1
            continue
        vsearchoutput_df = pd.read_csv(chimera_file, sep="\t", header=None, engine="python")
        vsearchoutput_df.columns = columns
        vsearchoutput_df_slim = vsearchoutput_df[
            (vsearchoutput_df.u6 == "100.0") & (vsearchoutput_df.u18 == "N") & (vsearchoutput_df.u1 >= 0.05)]
        shitlist = list(vsearchoutput_df_slim.u2)

        acceptable_recorde = list(filter_acceptable(shitlist, preliminary_file))
        yield out_file, acceptable_recorde
    logger.info(f"skipped regenerating {skipped} acceptable records")


def filter_chimeras(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (7) Runs VSEARCH for detecting and filtering chimeras.
    :param config:
    :param monitoring_df:
    :return:
    """
    # todo: this is only valid if collapsing (6) took place
    if MONITORING_SERIES_STEP_06 not in monitoring_df:
        logger.info('skipping chimera filtering, collapsing step did not take place')
        return
    singleton_series = monitoring_df[MONITORING_SERIES_STEP_06]
    if MONITORING_SERIES_STEP_07 not in monitoring_df:
        monitoring_df[MONITORING_SERIES_STEP_07] = pd.Series(index=monitoring_df.index, dtype='uint')
    for primer in config.primers:
        sub_folders = primer.folders.chimeras
        vsearchresults_folder = sub_folders.vsearchresults
        preliminary_folder = sub_folders.preliminary
        nonchimeric_folder = sub_folders.nonchimeric
        for subfolder in [vsearchresults_folder, preliminary_folder, nonchimeric_folder]:
            subfolder.mkdir(parents=True, exist_ok=True)

        collapsed_files = sorted_natural(primer.folders.collapse.collapsed.iterdir())

        files_to_search = list(find_files_that_need_vsearching(
            collapsed_files,
            singleton_series,
            primer_name=primer.name,
            vsearch_output_path=sub_folders.vsearchresults,
            prelim_output_path=sub_folders.preliminary
        ))
        for i, (collapsed_file, chimera_file, preliminary_file) in enumerate(files_to_search):
            show_progress(i, len(files_to_search), f'Chimera detection {primer.name}')
            proc = commands.run_vsearch_chimera(
                collapsed_input_file=collapsed_file,
                chimera_output_file=chimera_file,
                non_chimera_output_file=preliminary_file
            )
            err = [line.decode("utf-8") for line in proc.stderr.readlines()]
            proc.wait()
            if 0 != proc.returncode:
                for line in err:
                    if '' == line.strip():
                        continue
                    logger.error(line)
        preliminary_files: List[Path] = sorted_natural(sub_folders.preliminary.iterdir())
        records_to_write = generate_acceptable_records(
            preliminary_files,
            sub_folders.nonchimeric,
            sub_folders.vsearchresults
        )
        for out_file, records in records_to_write:
            try:
                with open(out_file, "wt") as outfasta:
                    for record in records:
                        SeqIO.write(record, outfasta, "fasta")
            except KeyboardInterrupt:
                logger.info(red_text('aborting'))
                if out_file.is_file():
                    out_file.unlink()
                    logger.info(f'removed partial file: {out_file}')

        logger.info(green_text(f"Chimera detection and filter done. Saving results for {primer.name}"))
        for non_chimeric_file in sub_folders.nonchimeric.iterdir():
            assert '.fasta' in non_chimeric_file.name
            indiv = non_chimeric_file.name.split(".fasta")[0]
            if (primer.name, indiv) in monitoring_df.loc[:, MONITORING_SERIES_STEP_07]:
                if 0 != monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_07]:
                    continue
            monitoring_df.loc[(primer.name, indiv), MONITORING_SERIES_STEP_07] = fasta_sum_size(non_chimeric_file)

    tmp_mon = config.root_folder / 'mon_tmp.csv'
    monitoring_df.to_csv(str(tmp_mon))
    tmp_mon.replace(config.root_folder / 'mon.csv')
