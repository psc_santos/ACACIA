import logging
from pathlib import Path

from Bio import SeqIO

from lib.output_helpers import red_text
from lib.config import AcaciaConfig

logger = logging.getLogger(__name__)


def convert_for_oligotype(config: AcaciaConfig) -> None:
    """
    (10) Converts the aligned files to proper format for entropy & oligotype, by expanding them.
    :param config:
    :return:
    """

    # depends on collapse-chimera-blasting-alignment

    for primer in config.primers:
        sub_folders = primer.folders.expand
        sub_folders.expanded.mkdir(exist_ok=True, parents=True)
        align_sub_folders = primer.folders.align
        for aligned_input in align_sub_folders.aligned.iterdir():
            convert(aligned_input, sub_folders.expanded / aligned_input.name)
        in_pool = align_sub_folders.pooled / 'pooled_aligned.fasta'
        out_pool = sub_folders.expanded / 'pooled.fasta'
        convert(in_pool, out_pool)


def convert(in_path: Path, out_path: Path) -> None:
    if out_path.is_file():
        return
    if not in_path.is_file():
        return
    relevant_records = []
    with open(str(in_path), "rt") as infasta:
        for record in SeqIO.parse(infasta, "fasta"):
            identifier = record.id.split("_")
            size = int(identifier[1])
            for i in list(range(size)):
                relevant_records.append(record)

    try:
        with open(str(out_path), "wt") as outfasta:
            for record in relevant_records:
                SeqIO.write(record, outfasta, "fasta")
    except KeyboardInterrupt:
        logger.info(red_text('aborting'))
        if out_path.is_file():
            out_path.unlink()
            logger.info('removed partial file:', str(out_path))
        raise
