# -*- coding: utf-8 -*-
import gzip
import logging
import subprocess
import sys
from multiprocessing import cpu_count as mp_cpu_count
from typing import Tuple
from pathlib import Path

CPU_COUNT = mp_cpu_count()
logger = logging.getLogger(__name__)


def run_fastqc(input_file: Path, output_dir: Path, cpu_count: int = CPU_COUNT) -> subprocess.Popen:
    command = ['fastqc', str(input_file), '-o', str(output_dir), '-t', str(cpu_count)]
    logger.debug(f"starting command: {command}")
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def run_flash(output_folder: Path,
              min_overlap: int, max_overlap: int,
              input_forward: Path, input_reverse: Path,
              max_mismatch_density: float = 0.1) -> Tuple[subprocess.Popen, str]:
    output_prefix = input_forward.name.split('_', 1)[0] + '.fastq'
    command = [
        'flash',
        '--allow-outies',
        '-x', str(max_mismatch_density),
        '-o', str(output_prefix),
        '-m', str(min_overlap),
        '-M', str(max_overlap),
        '--compress',
        '-d', str(output_folder),
        str(input_forward), str(input_reverse)
    ]
    logger.debug(f"starting command: {command}")
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT), output_prefix


def run_vsearch_collapse(input_file: Path, output_file: Path) -> subprocess.Popen:
    command = [
        'vsearch',
        '--derep_fulllength',
        str(input_file),
        '--minuniquesize', str(2),
        '--output', '-',  # output fasta on stdout
        '--relabel', 'seq',
        '--sizeout',
        '--quiet'
    ]
    logger.debug(f"starting command: {command}")
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    with gzip.open(str(output_file), 'wt') as gz_out:
        for line in proc.stdout.readlines():
            gz_out.write(line.decode("utf-8"))
    proc.wait()
    return proc


def run_vsearch_chimera(collapsed_input_file: Path, chimera_output_file: Path,
                        non_chimera_output_file: Path) -> subprocess.Popen:
    command = [
        'vsearch',
        '--abskew', str(4),
        '--alignwidth', str(0),
        '--mindiffs', str(1),
        '--uchime_denovo', str(collapsed_input_file),
        '--uchimeout', str(chimera_output_file),
        '--nonchimeras', str(non_chimera_output_file),
        '--quiet'
    ]
    logger.debug(f"starting command: {command}")
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def run_makeblastdb(input_file: Path, output_file: Path) -> subprocess.Popen:
    command = [
        'makeblastdb',
        '-in', str(input_file),
        '-parse_seqids',
        '-dbtype', 'nucl',
        '-out', str(output_file)  # rootfolder + "08_blast/" + primer + "/00_db/" + primer
    ]
    logger.debug(f"starting command: {command}")
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def run_blastn(db_path, non_chimeric_file, output_path):
    command = [
        'blastn',
        '-db', str(db_path),  # rootfolder + "08_blast/" + primer + "/00_db/" + primer
        '-query', str(non_chimeric_file),  # rootfolder + "07_chimeras/" + primer + "/03_nonchimeric/" + elem
        '-out', str(output_path),  # rootfolder + "08_blast/" + primer + "/01_blastresults/" + blastname
        '-max_target_seqs', str(1),
        '-outfmt', '10 qseqid evalue',
        '-num_threads', str(CPU_COUNT)
    ]
    logger.debug(f"starting command: {command}")
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def run_mafft(input_file: Path, output_file: Path) -> subprocess.Popen:
    command = [
        'mafft',
        '--auto',
        '--thread', str(CPU_COUNT),
        str(input_file),  # rootfolder + "09_align/" + primer + "/01_pooled/pooled.fasta
        # str(output_file)  # rootfolder + "09_align/" + primer + "/01_pooled/pooled_aligned.fasta"
    ]
    logger.debug(f"starting command: {command}")
    with open(str(output_file), 'wt') as outfile:
        proc = subprocess.Popen(command, stdout=outfile, stderr=sys.stderr)
        proc.wait()
        return proc
    # return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def run_entropy_analysis(input_file: Path) -> subprocess.Popen:
    command = [
        'entropy-analysis',
        '--quick',
        '--no-display',
        str(input_file),
    ]
    logger.debug(f"starting command: {command}")
    return subprocess.Popen(command, stdout=None, stderr=subprocess.PIPE)  # subprocess.PIPE


def run_oligotype(alignment_file: Path, entropy_file: Path, output_folder: Path,
                  selected_components: str, min_actual_abundance: int) -> subprocess.Popen:
    command = [
        'oligotype',
        str(alignment_file),  # rootfolder + "10_expand/" + primer + "/01_expanded/pooled.fasta"
        str(entropy_file),  # rootfolder + "11_entropy/" + primer + "/01_results/pooled.fasta-ENTROPY
        '-o', str(output_folder),  # rootfolder + "12_oligotype/" + primer + "/01_results/pooled
        '-C', str(selected_components),  # SELECTED_COMPONENTS
        '-s', str(1),  # MIN_NUMBER_OF_SAMPLES,
        '-a', str(3),  # MIN_PERCENT_ABUNDANCE,
        '-A', str(min_actual_abundance),
        '--skip-check-input-file',
        '--skip-basic-analyses',
        '--number-of-threads', str(CPU_COUNT),
    ]
    logger.debug(f"starting command: {command}")
    # return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STD)
    proc = subprocess.Popen(command)
    proc.wait()
    return proc
