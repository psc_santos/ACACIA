import logging
from pathlib import Path
from typing import List

import pandas as pd
from Bio import SeqIO

from lib.output_helpers import red_text, green_text
from lib import commands
from lib.config import AcaciaConfig
from lib.helpers import sorted_natural, show_progress
from lib.step_08 import MONITORING_SERIES_STEP_08

logger = logging.getLogger(__name__)


def align_fasta(config: AcaciaConfig, monitoring_df: pd.DataFrame) -> None:
    """
    (9) Creates and aligns pooled fasta files using MAFFT.
    Extract sequences from alignment in order to create individual aligned files.
    :param monitoring_df:
    :param config:
    :return:
    """
    # depends on collapse-chimera-blasting
    if MONITORING_SERIES_STEP_08 not in monitoring_df:
        logger.info('skipping sequence alignment, blasting did not take place')
        return

    for primer in config.primers:
        sub_folders = primer.folders.align
        for sub_folder in (sub_folders.aligned, sub_folders.pooled):
            sub_folder.mkdir(exist_ok=True, parents=True)
        cleaned_blast_files = sorted_natural(primer.folders.blast.blastclean.iterdir())
        pooled_fasta_path = sub_folders.pooled / 'pooled.fasta'
        pooled_aligned_fasta_path = sub_folders.pooled / 'pooled_aligned.fasta'
        build_pooled_fasta_file(pooled_fasta_path, cleaned_blast_files)
        if pooled_aligned_fasta_path.is_file():
            logger.info("pooled, aligned fasta file exists, not regenerating")
        else:
            commands.run_mafft(input_file=pooled_fasta_path, output_file=pooled_aligned_fasta_path)
        logger.info(green_text(f"Alignment done for {primer.name}"))
        files_to_build = list(find_interesting_files_that_need_to_be_built(cleaned_blast_files, sub_folders.aligned))
        for idx, (indiv_id, output_file_path) in enumerate(files_to_build):
            show_progress(idx + 1, len(files_to_build), f'Getting IDs aligned for: {primer.name}')
            try:
                with open(output_file_path, "wt") as outfasta:
                    for record in generate_interesting_records(pooled_aligned_fasta_path, indiv_id):
                        SeqIO.write(record, outfasta, "fasta")
            except KeyboardInterrupt:
                logger.info(red_text('aborting'))
                if output_file_path.is_file():
                    output_file_path.unlink()
                    logger.info(f'removed partial file: {output_file_path}')
                raise


def generate_pooled_records(cleaned_blast_files: List[Path]):
    for cleaned_blast_file in cleaned_blast_files:
        with open(cleaned_blast_file, "rt") as infasta:
            indiv = cleaned_blast_file.name.rstrip(".fasta")
            for record in SeqIO.parse(infasta, "fasta"):
                name = record.name
                identifier = name.replace("=", ";").split(";")
                size = int(identifier[2])
                record.id = f"{indiv}_{size}"
                record.name = record.description = record.id
                yield record


def find_ids_with_more_than_one_record(cleaned_blast_files: List[Path]):
    for index, cleaned_blast_file in enumerate(cleaned_blast_files):
        i = 0
        with open(cleaned_blast_file, "rt") as infasta:
            # we only want ids of files that contain more than one line
            for _ in SeqIO.parse(infasta, "fasta"):
                i += 1
                if i > 1:
                    yield cleaned_blast_file.name.rstrip(".fasta")
                    break


def build_pooled_fasta_file(output_path: Path, input_paths: List[Path]):
    if output_path.is_file():
        logger.info("pooled fasta file exists, skipping regeneration")
        return
    try:
        with open(output_path, "wt") as outfasta:
            for record in generate_pooled_records(input_paths):
                SeqIO.write(record, outfasta, "fasta")
    except KeyboardInterrupt:
        logger.info(red_text('aborting'))
        if output_path.is_file():
            output_path.unlink()
            logger.info(f'removed partial file: {output_path}')
        raise


def find_interesting_files_that_need_to_be_built(cleaned_blast_files: List[Path], output_folder: Path):
    already_built = 0
    for indiv_id in find_ids_with_more_than_one_record(cleaned_blast_files):
        output_file_path = output_folder / f'{indiv_id}.fasta'
        if output_file_path.is_file():
            already_built += 1
            continue
        yield indiv_id, output_file_path
    logger.info(f"skipped rebuilding {already_built} files")


def generate_interesting_records(pooled_aligned_fasta_path: Path, indiv_id):
    with open(pooled_aligned_fasta_path, "rt") as infasta:
        for record in SeqIO.parse(infasta, "fasta"):
            identifier = record.id.split("_")[0]
            if identifier == indiv_id:
                yield record
