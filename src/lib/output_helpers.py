import textwrap


class Bcolors:
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    END = "\033[0m"


def yellow_text(string):
    return Bcolors.YELLOW + string + Bcolors.END


def blue_text(string):
    return Bcolors.BLUE + string + Bcolors.END


def green_text(string):
    return Bcolors.GREEN + string + Bcolors.END


def red_text(string):
    return Bcolors.RED + string + Bcolors.END


ICON = """\

       ///
      *///////
       //////////     ///
        //////////    //         *////
          ////////// ./     ///////////
            //////// //   ////////////
              .////  // .////////////
                     // ///////////
                     ///////////
                    ///
                    //
                   ///
                   ///
                  ,///
                  ////
                  ////
                  ///.
                  ///
"""


def show_banner():
    print(blue_text(ICON))
    print(yellow_text("This is ACACIA: Allele Calling proCedure for Illumina Amplicon sequencing."))
    print(yellow_text(textwrap.dedent("""\
    ACACIA will call and name alleles out of your Illumnia raw data quickly and precisely.

    For things to run smoothly, you will need:
        1. Illumina paired-end raw data files (.fastq.gz format)
        2. Names and the exact sequences of your primers
        3. A fasta file with related sequences, in order to set up a local BLAST database

    """)))
    try:
        _ = input(yellow_text("Hit ENTER now if you are ready to go. " +
                              "Otherwise, press Ctrl + D to exit and " +
                              "restart later."))
    except EOFError:
        print("\n\n\nSee you soon!\n")
        exit(0)
