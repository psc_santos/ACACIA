# coding=utf-8
# -*- coding: utf-8 -*-
import gzip
import itertools
import os
import re
import sys
import shutil

from Bio import SeqIO
from typing import List, TypeVar, Iterable

T = TypeVar('T')


def sorted_natural(l: Iterable[T]) -> List[T]:
    def convert_to_int_or_lower(text):
        if text.isdigit():
            return int(text)
        return text.lower()

    def access_key_fn(text):
        return [convert_to_int_or_lower(c) for c in re.split("([0-9]+)", str(text))]

    return sorted(l, key=access_key_fn)


def unfold_primer(primer):
    """Unpacks an ambiguous DNA sequence into a list of all unambiguous oligos
    contained in that sequence"""
    primerdict = {"G": ["G"], "A": ["A"], "T": ["T"], "C": ["C"],
                  "R": ["A", "G"], "Y": ["C", "T"], "W": ["A", "T"],
                  "S": ["C", "G"], "M": ["A", "C"], "K": ["G", "T"],
                  "H": ["A", "C", "T"], "B": ["C", "G", "T"], "V": ["A", "C", "G"],
                  "D": ["A", "G", "T"], "N": ["A", "C", "G", "T"]}
    unfoldedprimers = list(map("".join, itertools.product(*map(primerdict.get, primer))))
    return unfoldedprimers


def rev_comp(seq):
    """Reverse complements a DNA sequence."""
    pairs = dict(zip("ACTGWSMKRYBDHVN", "TGACWSKMYRVHDBN"))
    return "".join(pairs[base] for base in seq[::-1])


def filter_quality(qualitylist, p, q):
    """ Calculates the percentage of bases (in a list of fastq phredscores
    from one read) that have quality equal or higher than q. Decides if read
    is good (1) or bad (0), based on threshold p"""
    good = 0
    if len(qualitylist) > 1:
        for item in qualitylist:
            if item >= int(q):
                good += 1
        goodpercentage = good / len(qualitylist) * 100
        if goodpercentage >= int(p):
            return 1
        else:
            return 0
    else:
        return 0


def sequence_compare(seq_a, seq_b):
    """Only to be used with pairs of previously aligned sequences. Returns
    the nr of mismatches between 2 sequences"""
    mismatches = 0
    len1 = len(seq_a)
    try:
        for pos in list(range(0, len1)):
            if seq_a[pos] != seq_b[pos]:
                mismatches += 1
        return mismatches
    except IndexError:
        return "Sequences being compared need to have the same length!"


def fasta_sum_size(fasta_path):  #
    """sums up values contained in the size parameter of fasta sequences"""
    k = 0
    with open(str(fasta_path), "rt") as infasta:
        for record in SeqIO.parse(infasta, "fasta"):
            name = record.id
            name = name.replace("=", ";")
            name = name.split(";")
            size = int(name[2])
            k += size
    return k


def count_records(path, seq_format):
    """counts the number of records in a gzipped fasta or fastq file"""
    with gzip.open(str(path), 'rt') as record_file:
        return len(list(SeqIO.parse(record_file, seq_format)))


def show_progress(iteration, total, prefix='', bar_length=20):
    """returns a nice progress bar"""
    try:
        col_width = shutil.get_terminal_size().columns
    except (ValueError, KeyError):
        col_width = 80  # 
    filled_length = int(round(bar_length * iteration / float(total)))
    percents = (iteration / float(total))
    bar = '█' * filled_length + '-' * (bar_length - filled_length)
    output_line = f'\r{prefix} |{bar}| {percents:>7.2%}'
    if len(output_line) > col_width:
        diff = len(output_line) - col_width
        output_line = f'\r{prefix} |{bar}| {percents:>7.2%}'
    else:
        diff = 1 + col_width - len(output_line)
        output_line += ' ' * diff

    sys.stdout.write(output_line),
    sys.stdout.flush()
    if iteration == total:
        sys.stdout.write('\n')
        sys.stdout.flush()
