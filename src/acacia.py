#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import lib
from lib.config import AcaciaConfig


def main():
    config = AcaciaConfig()
    # creates and begins to populate the monitoring dataframe
    config.check()

    if None is config.folders.root:
        # show banner only if there's no configuration, yet.
        # that way, acacia just starts running
        lib.show_banner()

    # monitoring_df is used throughout step 4-10
    monitoring_df = lib.build_monitoring_df(config)

    # Produces fastqc quality reports for each fastq file.
    lib.make_quality_reports(config)

    # Offers to trim out the ends of forward (R1) and reverse reads (R2),
    # based on the results of fastqc (2).
    # Produces fastqc quality reports for each new fastq file.
    config.check_trim()
    lib.trim_primers(config)
    config.check_merge()
    lib.merge_with_flash(config, monitoring_df)
    config.check_filter()
    lib.discard_trim_and_filter(config, monitoring_df)
    lib.collapse_sequences(config, monitoring_df)
    lib.filter_chimeras(config, monitoring_df)
    lib.blast_sequences(config, monitoring_df)
    lib.align_fasta(config, monitoring_df)
    lib.convert_for_oligotype(config)
    lib.analyze_entropy(config)
    lib.oligotype_analysis(config, monitoring_df)
    config.check_alleles()
    lib.extract_alleles(config, monitoring_df)


if __name__ == '__main__':
    try:
        logging.basicConfig(level=logging.INFO)
        main()
    except SystemExit as e:
        print('Exiting ACACIA', e.code)
    except KeyboardInterrupt:
        print('bye :D')
