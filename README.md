# ![ACACIA](logo/logo_web.png)

# ACACIA stands for "Allele CAlling proCedure for Illumina Amplicon sequencing data".

This workflow aims at extracting allele information out of paired-end Illumina FASTQC files. It does so by denoising, filtering and clustering sequences.
ACACIA will call and name alleles out of your raw data quickly and precisely.

## Installation:

ACACIA relies on some Python packages (notably [`Pandas`](https://pandas.pydata.org/) and [`Biopython`](https://biopython.org/)), as well as 6 pieces of third-party software (shown below). Due to constrains of some of them, this pipeline runs preferentially in a 64bit, UNIX-like system.

It is possible to run ACACIA in Windows from within a virtual machine ([`VirtualBox`](https://www.virtualbox.org/)). You can download the image of a fully installed system from [here](https://doi.org/10.6084/m9.figshare.9955208.v6). This is a good way to test the workflow.

In order to run ACACIA comfortably and taking advantage of all of your machine's capacity, I recommend following the instructions below (UNIX):

#### 1. Setup a new virtual environment for ACACIA:
Start by making sure you have [`conda`](https://docs.conda.io/en/latest/) ([`Anaconda`](https://anaconda.org/) or [`Miniconda`](https://docs.conda.io/en/latest/miniconda.html)) in your system. Open a terminal and type:

```sh
conda list
```
If you see a list of installed packages, you are good to go. Otherwise, you will need to install [`Anaconda`](https://anaconda.org/) (or [`Miniconda`](https://docs.conda.io/en/latest/miniconda.html), which is a minimal conda installer) first.

After that is done, open a terminal and type:

```sh
conda create -y --name acacia python=3.6
```
This command will create a virtual environment in your in system with the correct python version.

#### 2. Install 6 third-party software and setup the [`Bioconda`](https://bioconda.github.io/) channel:

First, activate the acacia virtual environment:

```sh
conda activate acacia
```

Installing [`R`](https://www.r-project.org/), [`FASTQC`](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/), [`FLASh`](https://ccb.jhu.edu/software/FLASH/), [`VSEARCH`](https://github.com/torognes/vsearch/), [`BLAST`](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download) and [`MAFFT`](https://mafft.cbrc.jp/alignment/software/) is extremely easy. Just type:

```sh
conda install -y r-base fastqc flash vsearch blast mafft -c bioconda
```

Once that was successful, install pandas and some R packages:

```sh
conda install -y pandas r-ggplot2 r-vegan r-gplots r-gtools r-reshape r-optparse r-pheatmap r-rcolorbrewer r-compute.es
```
#### 3. Install [`Oligotyping`](http://merenlab.org/software/oligotyping/):
This is ACACIA's crucial dependency. Install it by typing:

```sh
pip install oligotyping
```

#### 4. Get ACACIA:
Create a folder to install ACACIA in and navigate to that folder:

```sh
mkdir ~/ACACIA && cd $_
```

Make sure you have git installed. If you do not, you can install it with:
```sh
sudo apt-get update
sudo apt-get install git
```

Download (clone) this repository by typing:

```sh
git clone https://gitlab.com/psc_santos/ACACIA.git
```

#### 5. Start ACACIA:
Navigate (`cd`) to the folder where ACACIA was installed and start ACACIA with:

```sh
python acacia.py
```

For future invocations you may also start ACACIA by providing the directory via

```sh
python acacia.py /path/to/your/work/directory
```

you may also check for other arguments on the command line:

```sh
python acacia.py --help
```

You will always need to activate the acacia virtual environment prior to starting ACACIA:
```sh
conda activate acacia
```

ACACIA will create a config file (`config.ini`) for each pipeline run.
You are welcome to edit this file in order to change any parameters and avoid ACACIA asking you for settings again, if you are repeating a pipeline run.

You can stop the workflow at any point (by pressing `Ctrl + C`) and restart the process later by typing `python acacia.py` again and indicating the existing data folder. In that case, ACACIA will try to continue the allele calling process by looking into the previously created folders and into the `config.ini` file, in order to choose at which stage the pipeline should be resumed. [Here is an example of a complete `config.ini`](config.ini) file.

#### 6. Repeat ACACIA with different settings:
It is likely that the optimal `abs_nor` and `low_por` settings for each dataset will vary, [which is discussed in detail here](https://onlinelibrary.wiley.com/doi/full/10.1111/1755-0998.13290?af=R). Using the default settings of `abs_nor` and `low_por` without checking whether they are optimal for each specific dataset is **NOT** recommended. You should try a range of `abs_nor` and `low_por` settings to deduce what is likely to be the optimal settings for your own dataset.

To repeat an analysis with different `abs_nor` and `low_por` settings without repeating all the other steps in the pipeline and whilst keeping results from previous analyses:

* Navigate (`cd`) to the folder where your ACACIA output was saved.

* Change the name of the folder "13_reports" in your ACACIA output directory to a different name. Failing to do so will result in you losing the results of your previous analysis when rerunning with different settings. You can do this either by right-clicking the folder and choosing rename **or** in the terminal (*e.g. if abs_nor = 0.01 and low_por = 100*):

```sh
mv 13_reports 13_reports_abs_nor_100_low_por_0.01
```

* Change the settings of `abs_nor` and `low_por` in the configuration file `config.ini` using nano or your favourite text editor:

```sh
nano config.ini
```

* Once the new settings in the configuration file `config.ini` are set, rerun ACACIA:

```sh
python acacia.py
```

* Remember to change the name of the folder "13_reports" in your ACACIA output directory before running further analyses with different settings and repeating the above steps.


## To run the pipeline, you will also need:
* Your paired-end raw Illumina FASTq files ([example files here](https://figshare.com/s/017ee3dae46edc656c08)).

* The names and exact sequences of your template-specific primers ([example here](https://figshare.com/s/9f40d994cd39a02e9998)).

* A fasta file with 100+ sequences related to those that you expect to have sequenced ([here is an example](https://figshare.com/s/3148ef72590db2c47acc)). This file will be used by ACACIA to setup a local BLAST database.


## Citing & Documentation:
ACACIA is described with detail [in this paper](https://onlinelibrary.wiley.com/doi/full/10.1111/1755-0998.13290?af=R). The article has not yet been peer-reviewed, but you are most welcome to cite it and also to comment!


## Contributing:

Contributions, even very small ones, are greatly appreciated. If you want to join the project, here is one way to do it:

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/NewFeature`)
3. Commit your Changes (`git commit -m 'Add some NewFeature'`)
4. Push to the Branch (`git push origin feature/NewFeature`)
5. Open a Merge Request



## Forum:

For any troubleshooting issues please send your questions to the following google group:

https://groups.google.com/forum/#!forum/acacia_pipeline_qa


