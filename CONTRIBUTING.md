Contributions, even very small ones, are greatly appreciated. If you want to join the project, here is one way to do it:

* Fork the Project
2. Create your Feature Branch (`git checkout -b feature/NewFeature`)
3. Commit your Changes (`git commit -m 'Add some NewFeature'`)
4. Push to the Branch (`git push origin feature/NewFeature`)
* Open a Merge Request
