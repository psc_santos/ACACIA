FROM continuumio/miniconda3:4.11.0 as buildenv
ENV LANG C.UTF-8
COPY environment.yml .
RUN --mount=id=conda,type=cache,target=/opt/conda/pkgs \
    conda env create --name acacia --file environment.yml && \
    conda install conda-pack
RUN conda-pack -n acacia -o /env.tgz

FROM continuumio/miniconda3:4.11.0 as unpacked
COPY --from=buildenv /env.tgz /env.tgz
RUN tar xf /env.tgz -C /opt/conda && rm /env.tgz
RUN conda-unpack

FROM unpacked
WORKDIR /app
ENV PYTHONIOENCODING utf-8
# if these are not set, fastqc will break, because fontconfig will search in the wrong place
ENV FONTCONFIG_FILE /opt/conda/etc/fonts/fonts.conf
ENV FONTCONFIG_PATH /opt/conda/etc/fonts
COPY src/ ./
VOLUME ["/data"]
CMD ["python", "acacia.py", "/data"]
#CMD ["python", "-u", "acacia.py", "/data"]

