if at some time oligotyping receives an update, the following is recommended to regenerate the conda-lock files:

```bash
python -m venv{,}
. ./venv/bin/activate
pip install conda-lock[pip_support]
conda lock -p linux-64
deactivate
rm -rf venv
```

```bash
source /opt/anaconda/bin/activate root
conda activate acacia
```
 
